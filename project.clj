(defproject crossref-agreements "0.1.0-SNAPSHOT"
  :description "CrossRef Agreements Service"
  :plugins [[lein-ring "0.8.10"] [lein-daemon "0.5.4"]]
  :dependencies [[org.clojure/clojure "1.5.1"]
              [korma "0.3.0-RC5"]
              [org.postgresql/postgresql "9.2-1002-jdbc4"]   
              [mysql-java "5.1.21"]
              [clj-http "0.9.0"]
              [clj-time "0.6.0"]
              [org.clojure/data.json "0.2.4"]
              [org.clojure/tools.logging "0.2.6"]
              [lein-ring "0.8.10"]
              [http-kit "2.1.10"]
              [javax.servlet/servlet-api "2.5"]
              [org.clojure/clojure "1.5.1"]
              [compojure "1.1.6"]
              ; [ring-middleware-format "0.3.2"]
              [liberator "0.11.0"]
              [oauthentic "1.0.1"]]

  :main ^:skip-aot clickthrough.core
  :target-path "target/%s"
  :ring {:handler clickthrough.core/app}
  :profiles {:uberjar {:aot :all}}
  :daemon {:clickthrough {:ns clickthrough.core :pidfile "clickthrough.pid"}})
