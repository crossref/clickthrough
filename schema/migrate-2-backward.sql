alter table crossRefUserAccessToken change retiredAt retiredAt timestamp not null;
alter table orcidUserAccessToken change retiredAt retiredAt timestamp not null;