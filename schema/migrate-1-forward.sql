-- A publisher using a Community License.

CREATE TABLE publisherUsesLicense (
    id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    crossRefUser INTEGER NOT NULL REFERENCES crossRefUser(id),
    license INTEGER NOT NULL REFERENCES license(id)
);

-- A publisher's name.

ALTER TABLE crossRefUser ADD COLUMN publisherName TEXT;

