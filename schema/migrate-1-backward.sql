-- A publisher using a Community License.

DROP TABLE publisherUsesLicense;

-- A publisher's name.

ALTER TABLE crossRefUser DROP COLUMN publisherName;

