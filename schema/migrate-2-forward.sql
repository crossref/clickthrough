-- Make the tokens' retired-date fields nullable

alter table crossRefUserAccessToken change retiredAt retiredAt timestamp null;
alter table orcidUserAccessToken change retiredAt retiredAt timestamp null;