alter table crossRefUserAccessToken modify column retiredAt timestamp null default null;
alter table orcidUserAccessToken modify column retiredAt timestamp null default null;