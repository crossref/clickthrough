-- Orcid User

CREATE TABLE orcidUser (
    id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    orcid VARCHAR(19) UNIQUE NOT NULL
);


CREATE UNIQUE INDEX OrcidUser_pk ON orcidUser (id);
CREATE UNIQUE INDEX OrcidUser_Orcid ON orcidUser (orcid);

-- CrossRef user

CREATE TABLE crossRefUser (
    id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    username VARCHAR(255) unique NOT NULL,
    isCrossRefStaff bool NOT NULL default false
);

CREATE UNIQUE INDEX crossRefUser_pk ON crossRefUser (id);
CREATE UNIQUE INDEX crossRefUser_Username ON crossRefUser (username);

-- License

-- The uri field is unique but MySQL doesn't allow unique on long text. 
-- MyISAM only allows 1000-long, InnoDB only allows 767.
-- So the uniqueness is checked in code in a transaction.
CREATE TABLE license (
    id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    title TEXT,
    uri TEXT NOT NULL,
    fullLicenseText LONGTEXT,
    description LONGTEXT,
    createdBy INTEGER REFERENCES crossRefUser(id),
    status INTEGER NOT NULL default 0
) ;

CREATE UNIQUE INDEX license_pk ON license (id);
CREATE UNIQUE INDEX license_uri ON license (uri(767));

-- Review of license by Researcher

CREATE TABLE review (
    id INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    orcidUser INTEGER NOT NULL REFERENCES orcidUser(id),
    license INTEGER NOT NULL REFERENCES license(id),
    status INTEGER NOT NULL,
    reviewedAt TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
);

CREATE UNIQUE INDEX review_pk ON review(id);

-- Tokens for Researchers (OrcidUsers) and for Members (CrossRefUser).

CREATE TABLE orcidUserAccessToken (
    id  INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    value VARCHAR(255) UNIQUE NOT NULL,
    belongsTo INTEGER REFERENCES orcidUser(id),
    current BOOLEAN default false,
    issuedAt TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    retiredAt TIMESTAMP 
);

CREATE UNIQUE INDEX orcidUserAccessTokenIndex on orcidUserAccessToken(value, belongsTo, current);

CREATE TABLE crossRefUserAccessToken (
    id  INTEGER NOT NULL PRIMARY KEY AUTO_INCREMENT,
    value VARCHAR(255) UNIQUE NOT NULL,
    belongsTo INTEGER REFERENCES crossRefUser(id),
    current BOOLEAN default false,
    issueAt TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    retiredAt TIMESTAMP 
);

CREATE UNIQUE INDEX crossRefUserAccessTokenIndex on crossRefUserAccessToken(value, belongsTo, current);