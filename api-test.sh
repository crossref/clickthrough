# These values correspond to config.dev.edn (read by the Clojure codebase) and config.api.test.json (read by the Python API tests).

set +e
mysql -u root  -e "drop database licenses_api_test"
set -e

mysql -u root -e "create database licenses_api_test"

mysql -u root licenses_api_test < schema/schema.sql
mysql -u root licenses_api_test < schema/migrate-1-forward.sql
mysql -u root licenses_api_test < schema/migrate-2-forward.sql

echo "RUN TESTS"

# Now run the API tests.

# To run specitic tests, append class and optionally  test name.
# e.g. python python/api-test.py TestLicensesMember.
# e.g. python python/api-test.py TestLicensesMember.test_can_get_global_licenses

python python/api-test.py 