(ns clickthrough.consts
  (:require [clojure.set :refer [map-invert]]))

; Consts used in DB and in API.

; These aren't symbols because of the extra overhead of transalting back and forth between the database.
; TODO add license- prefix
(def draft-state 0)
(def published-state 1)
(def retired-state 2)
(def deleted-state 3)

(def license-states [draft-state published-state retired-state deleted-state])


; Triples of [from-state, to-state, are-there-any-researcher-reviews]. 
; Iff a match is made, the transition is allowed. 
(def allowed-license-transitions #{
  [draft-state draft-state false]
  [draft-state published-state false]
  [draft-state deleted-state false]
  
  [published-state draft-state false]
  [published-state published-state true]
  [published-state published-state false]
  [published-state retired-state true]
  [published-state retired-state false]
  
  [retired-state published-state true]
  [retired-state published-state false]
  [retired-state retired-state true]
  [retired-state retired-state false]
  
  ; [deleted-state published-state true]
  ; [deleted-state published-state false]
  ; [deleted-state deleted-state true]
  ; [deleted-state deleted-state false]
  })

(def review-unreviewed-state 0)
(def review-postponed-state 1)
(def review-rejected-state 2)
(def review-accepted-state 3)

(def review-state-names {
  review-unreviewed-state "unreviewed" 
  review-postponed-state "postponed" 
  review-rejected-state "rejected" 
  review-accepted-state "accepted" })

(def review-state-names-inverse (map-invert review-state-names))

(def review-states [review-unreviewed-state review-postponed-state review-rejected-state review-accepted-state])

; Tuples of from-review-state to to-review-state. 
; Iff a match is made, transition is allowed.
(def allowed-review-transitions #{
  [review-unreviewed-state review-unreviewed-state]
  [review-unreviewed-state review-postponed-state]
  [review-unreviewed-state review-rejected-state]
  [review-unreviewed-state review-accepted-state]
  [review-postponed-state review-unreviewed-state]
  [review-postponed-state review-postponed-state]
  [review-postponed-state review-rejected-state]
  [review-postponed-state review-accepted-state]
  [review-rejected-state review-accepted-state]})
