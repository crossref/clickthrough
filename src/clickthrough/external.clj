(ns clickthrough.external
  (:require [clj-http.client :as client])
  (:require [oauthentic.core :as oauth])
  (:require [clickthrough.config :refer [config]]))

; Talking to external APIs.

(def api-base (or (config :api-base) "https://apps.crossref.org/clickthrough/api/"))

; The URL where the user can be redirected to view the license.
(def reviewers-base-url (or (config :reviewers-base-url) "https://apps.crossref.org/clickthrough/researchers/tc/"))

; For CORS
(def allowed-origin (or (config :allowed-origin) "https://apps.crossref.org"))

; These are production defaults. In development they're over-ridden by the environment.
(def crossref-login-url (or (config :crossref-login-url) "https://auth.labs.crossref.org/login"))
(def orcid-scope (or (config :orcid-scope) "/orcid-profile/read-limited"))
(def orcid-fetch-profile-url (or (config :orcid-fetch-profile-url) "https://api.orcid.org/%s/orcid-profile"))

; These can't be embedded and always come from the environment.
(def orcid-client-id (config :orcid-client-id))
(def orcid-client-secret (config :orcid-client-secret))
(def orcid-authorize-url (or (config :orcid-authorize-url) "https://orcid.org/oauth/authorize"))

(def orcid-token-url (or (config :orcid-token-url) "https://api.orcid.org/oauth/token"))

; This route is passed into the ORCID OAuth2 query, and must be matched in the routing. 
; As it's configurable, the route value must too be.
(def orcid-redirect-url-route (config :orcid-redirect-url-route))
(def orcid-redirect-url (config :orcid-redirect-url))

(def orcid-full-auth-url (oauth/build-authorization-url orcid-authorize-url { :client-id orcid-client-id :redirect-uri orcid-redirect-url :scope orcid-scope}))

(defn log-in-crossref
  "Log in against the CrossRef API. Returns true, false or :error."
  [username password]
    (try
      (let [result (client/get crossref-login-url {:insecure? true :headers {"CR-USERNAME" username "CR-PASSWORD" password} :throw-exceptions false})]
        (= (:status result) 200))
    (catch java.net.ConnectException e :error)))

; Check the ORCID authentication code and return the ORCID or nil on error.
(defn fetch-authenticated-orcid [code]
  (try
      (let [response (oauth/fetch-token orcid-token-url { :scope orcid-scope :grant_type "authorization_code" :client-id orcid-client-id :client-secret orcid-client-secret :code code :redirect-uri orcid-redirect-url})
            
            ; Igonre the token. We just want the ORCID.
            orcid (:orcid response)]
        orcid)
    ; If the response is an error not not valid JSON, this happens.
    ; Represent this as 'authentication failed'.
    (catch java.lang.ClassCastException e nil)))


(def orcid-redirect-html
"<!doctype html>
<html><script type=\"text/javascript\">
    if (window.parent) {
        window.parent.postMessage(\"ok\", \"*\");    
    }
    
    if (window.opener) {
        window.opener.postMessage(\"ok\", \"*\");    
    }

    window.close();
</script></html>")

(def orcid-redirect-html-bad
"<!doctype html>
<html><script type=\"text/javascript\">
    if (window.parent) {
        window.parent.postMessage(\"orcid-error\", \"*\");    
    }
    
    if (window.opener) {
        window.opener.postMessage(\"orcid-error\", \"*\");    
    }

    window.close();
</script></html>")