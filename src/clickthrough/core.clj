(ns clickthrough.core
  (:require [clojure.core :refer [format]])
    
  (:require [clickthrough.consts :refer :all]
            [clickthrough.external :as external]
            [clickthrough.database :as db]
            [clickthrough.entities :refer :all]
            [clickthrough.config :refer [config]])
  
  (:require [clojure.string]
            [clojure.tools.logging :refer [info]]
            [clojure.data.json :as json])
 
  (:require [compojure.handler :as handler]
            [compojure.route :as route]
            [compojure.core :refer [defroutes routes context GET PUT POST DELETE ANY]])
  
  (:require [org.httpkit.server :as hs])
 
  (:require [ring.middleware.session :as session]
            [ring.util.response :refer [response redirect status content-type header]]
            [ring.middleware.session.cookie :refer [cookie-store]])
  
  (:require [korma.db :refer [defdb mysql transaction]])

  (:require [liberator.core :refer [defresource resource]])
   
  (:require [liberator.dev :refer [wrap-trace]])
  (:gen-class))


; Prefix a given string to each URL in the routes, so this can be reverse-proxied.
(def p (partial str (:route-prefix config)))

(defn to-json [input] (json/write-str input))
(defn from-json [input] (json/read-str input :key-fn keyword))

; defresource wrapped in a transaction.
; takes an extra key, :handle-failed-transaction that takes a function that should return a ring response.
(defmacro deftxresource [name & kvs]  
  (if (vector? (first kvs))
    (let [args (first kvs)
          kvs (rest kvs)
          args-as-map (apply hash-map kvs)
          transaction-fn (or (:handle-failed-transaction args-as-map) (fn [_] ))]
      `(defn ~name [~@args]
         (try
           (let [result# (transaction (resource ~@kvs))]
             result#)
           
           (catch com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException e#
            (~transaction-fn e#)))))
    (let [args-as-map (apply hash-map kvs)
          transaction-fn (or (:handle-failed-transaction kvs) (fn [_]))]
      `(def ~name 
           (try
             (transaction (resource ~@kvs))
             (catch ~com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException e#
              (~transaction-fn e#)))))))

(defn assoc-if-not 
  [coll k v]
  (assoc coll k (or (get coll k) v)))

; Nomenclature
; Due to a some vocabulary changes,
; License == Terms and Conditions == Agreement
; Member == Publisher
; Prospector == Researcher

; Session store key for ORCID id.
(def session-orcid-id :session-orcid-id)

; Session store key for CrossRef id. 
(def session-crossref-id :session-crossref-id)

(defn wrap-crossref-session
  "Add :crossref-user into request if logged in."
  [handler]
  (fn [request]
    (let [crossref-id (-> request :session session-crossref-id)
          user (when-not (nil? crossref-id) (db/get-or-create-crossref-user crossref-id))]
          (handler (assoc request :crossref-user user)))))

(defn wrap-orcid-session
  "Add :orcid-user into request if logged in or add :orcid-login-url."
  [handler]
  (fn [request]
    (let [orcid-id (-> request :session session-orcid-id)
          orcid-user (when-not (nil? orcid-id) (db/get-or-create-orcid-user orcid-id))]
          (if (nil? orcid-user)
            (handler (assoc request :orcid-login-url external/orcid-full-auth-url))
            (handler (assoc request :orcid-user orcid-user))))))

(defn wrap-cors [handler]
                (fn [request]
                    (let [resp (handler request)
                          headers (:headers resp)
                          with-new-headers (assoc resp :headers (-> headers
                                               (assoc "Access-Control-Allow-Methods" "GET, POST, PUT, OPTIONS, DELETE")
                                               (assoc "Access-Control-Allow-Headers" "Access-Control-Allow-Origin, Access-Control-Allow-Credentials, Accept, Origin, Content-Type")
                                               (assoc "Access-Control-Allow-Credentials" "true")
                                               (assoc "Access-Control-Allow-Origin" external/allowed-origin)))]
                      (when resp with-new-headers))))


(defn arg-count [f]
  (let [m (first (.getDeclaredMethods (class f)))
        p (.getParameterTypes m)]
    (alength p)))


(defn authenticate-crossref-and
  "Check the authentication status of a CrossRef user and execute the function with [user] if so."
  [context fun]
  (let [is-options (= (-> context :request :request-method) :options)
           user (get-in context [:request :crossref-user])]
    (if (or is-options user) (fun user))))

(defn authenticate-orcid-and
  "Check the authentication status of a ORCID user and execute the function with [user] if so."
  [context fun]
  (let [is-options (= (-> context :request :request-method) :options)
        user (get-in context [:request :orcid-user])]
    (if (or is-options user) (fun user))))

(deftxresource member-license
  [id]
  :allowed-methods [:put :get :options]
  :available-media-types ["application/json"]
  
  :authorized? (fn [context] (authenticate-crossref-and context (fn [user] {::user user})))
  
  :handle-unauthorized (fn [context] (to-json {:status "Not authorized"}))
  
  :allowed? (fn [context]
              (let [is-options (= (-> context :request :request-method) :options)
                    id (Integer/parseInt id)
                    previous-license (db/get-license-for-member id)
                    exists? (not (nil? previous-license))
                    owned? (= (:createdBy previous-license) (:id (::user context)))]
                  (when (or is-options (and exists? owned?)) {::previous-license previous-license})))
  
  ; if we've got past allowed? then it must exist, but make explicit.
  :exists? (fn [context] (not (nil? (::previous-license context))))
  
  ; Previous license is already in context, in internal form.
  :handle-ok (fn [context] 
               (info "get member-license" id "ok")
               (to-json (export-license (::previous-license context) (::user context))))

  ; put may fail if the state transition is invalid or there are reviews
  :conflict? (fn [context] (let [
          user (::user context)
          previous-license (::previous-license context)
          license (from-json (slurp (get-in context [:request :body])))
          
          ; Convert license from API-form to internal form.
          imported-license (import-license license)
          
          ; If some fields weren't submitted, fill them in from the previous version.
          ; Case in point :createdBy, :state, :id.
          full-license (merge previous-license imported-license)
          safe-license (escape-license full-license)
          validation-errors (validate-license safe-license (:id user))
          num-reviews (+ (:accepts previous-license) (:postpones previous-license) (:rejects previous-license))

          transition-allowed (validate-license-transition previous-license safe-license (> num-reviews 0))          
          
          ; Re-fetch by URI. If there's already a different license by this URI, that's a conflict.
          license-by-uri (db/get-license-by-url (:uri safe-license))
          uri-duplicate (not (or (nil? license-by-uri) (= (:id license-by-uri) (:id safe-license))))
          
          return-error (cond
            uri-duplicate "Duplicate URI" 
            (false? transition-allowed) "State transition not allowed"
            (not (empty? validation-errors)) "Input did not validate"
            
            :else nil)]
      
      (if return-error
        [true {::safe-license safe-license ::conflict-error return-error}]
        [false {::safe-license safe-license}])))

  :handle-conflict (fn [context] (::conflict-error context))
  
  :put! (fn [context]
    (info "update member-license" id "ok")
    (db/update-license id (::safe-license context) (::user context))))

(deftxresource member-licenses
  []
  :available-media-types ["application/json"]
  :allowed-methods [:get :post :options]
  
  :malformed? (fn [context]
                ; Ignore for GET.
                (when (= :post (-> context :request :request-method))
                  (let [user (::user context)
                        license (from-json (slurp (get-in context [:request :body])))
                        imported-license (import-license license)
                        
                        ; Unlike the PUT, we're not going to merge in the :createdBy here because we need to authenticate first. 
                        ; That happens in post!
                        full-license (-> imported-license (assoc :status draft-state))
                        safe-license (escape-license full-license)
                        validation-errors (validate-license safe-license (:id user))

                        ; if there's already an existing license, that's a problem
                        license-by-uri (db/get-license-by-url (:uri safe-license))
                        error (cond
                                (not (empty? validation-errors)) "Input did not validate"
                                (not (nil? license-by-uri)) "Another license with that URI exists."
                                :else nil)]
                    (if error [true {::error error}] [false {::safe-license safe-license}]))))
  
  :handle-malformed (fn [context] (to-json {:status "error", :error (::error context)}))
  
  :authorized? (fn [context]
                 (authenticate-crossref-and context
    (fn [user] {::user user})))
  
  :handle-unauthorized (fn [context] (to-json {:status "Not authorized"}))
  
  :post! (fn [context] (let [license (::safe-license context)
                                          license-to-insert (assoc license :createdBy (:id (::user context)))
                                          created-license (db/create-license license-to-insert)
                                          ; The returned internal format needs converting to exported format.
                                          exported-license (export-license created-license (::user context))]
                         (info "add member-license ok")
                         {::exported-created-license exported-license}))

  :handle-created (fn [context] (::exported-created-license context))
  
  :handle-ok (fn [context]
        (info "get member-licenses")
        (to-json (map #(export-license % (::user context)) (db/get-licenses (::user context))))))
  
  
(defresource publisher-token
  []
  :allowed-methods [:get :delete :options]
  :available-media-types ["text/plain"]
  
  :authorized? (fn [context]
                 (authenticate-crossref-and context
    (fn [user] {::user user})))
  
  :handle-unauthorized (fn [context] 
                         (info "get member token not authorized")
                         "Not authorized")
  
  :handle-ok (fn [context]
    (let [user (::user context)
          token (db/get-crossref-user-token user)]
      (if-not (nil? token)
        ; Return or create.
        (:value token)
      
        ; Need to create one.
        (let [new-token (generate-token)]
          (db/insert-crossref-user-token user new-token)
          
          (info "get member token for user id" (:id user) "ok")
          new-token))))
  
  :delete! (fn [context] (db/revoke-crossref-user-token (::user context))
             (info "delete member token for user id " (:id (::user context)) "ok")))

(defresource researcher-token
  []
  :allowed-methods [:get :delete :options]
  :available-media-types ["text/plain"]
  
  :authorized? (fn [context] (authenticate-orcid-and context
    (fn [user] {::user user})))
  
  :handle-unauthorized (fn [context]
                         (info "get researcher token not authorized")
                         external/orcid-full-auth-url)
  
  :handle-ok (fn [context]
    (let [user (::user context)
          token (db/get-orcid-user-token user)]
      (if-not (nil? token)
        ; Return or create.
        (:value token)
      
        ; Need to create one.
        (let [new-token (generate-token)]
          (db/insert-orcid-user-token user new-token)
          (info "get researcher token for user id" (:id user) "ok")
          new-token))))
  
  :delete! (fn [context] (db/revoke-orcid-user-token (::user context))))


(defresource publisher-me
  []
  :allowed-methods [:get]
  :available-media-types ["application/json"]
  
  :authorized? (fn [context]
                 (authenticate-crossref-and context
    (fn [user] {::user user})))
  
  :handle-unauthorized (fn [context] 
                         (info "publisher 'about me' not authroized")
                         (to-json {:status "not authorized"}))
  
  :handle-ok (fn [context]
    (let [user (::user context)]
          (info "get publisher 'about me'" (:id user) "ok")
          (to-json {:status "ok" :user user}))))
  
  
(defresource researcher-me
  []
  :allowed-methods [:get]
  :available-media-types ["application/json"]
  
  :authorized? (fn [context]
                 (authenticate-orcid-and context
    (fn [user] {::user user})))
  
  :handle-unauthorized (fn [context] 
                         (info "researcher 'about me' not authroized")
                         (to-json {:status "not authorized"}))
  
  :handle-ok (fn [context]
    (let [user (::user context)]
          (info "get researcher 'about me'" (:id user) "ok")
          (to-json {:status "ok" :user user}))))

(def widget-html
"<html>
  <head>
  </head>
  <body>
    <h1>%s</h1>
    <a href=\"%s\">Agree to this license</a>
  </body>
</html>")

(defresource embed-for-uri
  [uri]
  :allowed-methods [:get]
  :available-media-types ["text/html"]
  :exists? (fn [context]
             (let [license (db/get-license-for-no-researcher-by-url uri)
                   license-status (:status license)
                   
                   ; Only show published and retired licenses to researchers.
                   license-status-valid (or (= license-status published-state) (= license-status retired-state))]
               (when license-status-valid {::license license})))
    :handle-ok (fn [context] (let [license (::license context)]
                                   response (format
                                              widget-html
                                              (:title license)
                                              (str external/reviewers-base-url (:uri license))))))

(defresource embed-for-doi
  [doi]
  
  
)

(defresource license-review 
  [uri]
  :allowed-methods [:get :put :options] 
  :available-media-types ["application/json"]

  :authorized? (fn [context] (authenticate-orcid-and context
    (fn [user] {::user user})))
  
  :handle-unauthorized (fn [context] (to-json {:status "Not authorized" :orcid-login-url external/orcid-full-auth-url}))
  
  :exists? (fn [context]
             (let [license (db/get-license-for-researcher-by-url (::user context) uri)
                   license-status (:status license)
                   
                   ; Only show published and retired licenses to researchers.
                   license-status-valid (or (= license-status published-state) (= license-status retired-state))]
               (when license-status-valid {::license license})))

  :malformed? (fn [context]
                (when (= (-> context :request :request-method) :put)
                  (let [sent-license (from-json (slurp (get-in context [:request :body])))
                        new-review-status-name (:status sent-license)    
                        new-review-status (get review-state-names-inverse new-review-status-name)]
                   ; We only care about whether we can recognised the status.
                   (if new-review-status 
                     [false {::new-review-status new-review-status}]
                     [true {::error "Invalid review status sent."}]))))
  
  :handle-malformed (fn [context]
                      (info "malformed license review for uri" uri)
                      (to-json {:status "error", :error (::error context)}))

  :conflict? (fn [context]
               (let [old-review-status (:review-status (::license context))
                     new-review-status (::new-review-status context)]
                (not (get allowed-review-transitions [old-review-status new-review-status]))))
              
  :handle-ok (fn [context]
               (info "get license review for uri" uri "ok")
               (export-license-review (::license context) nil))
  
  :put! (fn [context]
                (db/perform-review (::license context) (::new-review-status context) (::user context))
                (info "perform license review for uri" uri "ok")))



(defresource license-reviews
  [state]
  
  :allowed-methods [:get :options] 
  :available-media-types ["application/json"]

  :authorized? (fn [context] (authenticate-orcid-and context
    (fn [user] {::user user})))

  :handle-unauthorized (fn [context] (to-json {:status "Not authorized" :orcid-login-url external/orcid-full-auth-url}))
  
  :exists? (fn [context]
            (when-let [the-state (get review-state-names-inverse state)]
              {::state the-state}))
  
  :handle-ok (fn [context]
               (let [the-licenses (db/get-all-licenses-for-researcher (::user context))
                     in-state (filter #(= (:review-status %) (::state context)) the-licenses)]
                 (info "get license reviews in state" state "for researcher user" (:id (::user context)) " ok")
                 (map #(export-license-review % nil) in-state))))

(defresource member-global-license
  [id]
  :allowed-methods [:get :put :options]
  :available-media-types ["application/json"]
  :authorized? (fn [context] (authenticate-crossref-and context (fn [user] {::user user})))
  
  :handle-unauthorized (fn [context] (to-json {:status "Not authorized" }))
  
  :exists? (fn [context] (let [id (Integer/parseInt id)
                               license (db/get-license id)
                               is-published (is-license-in-state license published-state)]
                           (when (and license is-published) {::license license})))
  
  :handle-ok (fn [context] 
               (let [license (::license context)
                     is-used (db/is-license-used (::license context) (::user context))
                     license-used (assoc license :used is-used)]
                     (info "get member global license use id" id "for user" (:id (::user context)) "ok")
               (export-license license-used (::user context))))
  
  :malformed? (fn [context]
                ; Ignore for GET.
                (when (= :put (-> context :request :request-method))
                  (let [sent-license (from-json (slurp (get-in context [:request :body])))
                        used (:used sent-license)]
                    (if sent-license
                      [false {::sent-license sent-license ::used used}]
                      true))))

  :put! (fn [context] 
          (info "set member global license id" id "for user" (:id (::user context)) "ok")
         (db/set-license-use (::license context) (::user context) (::used context))))


(defresource member-global-licenses
  []
  :allowed-methods [:get :options]
  :available-media-types ["application/json"]
  :authorized? (fn [context] (authenticate-crossref-and context (fn [user] {::user user})))
  
  :handle-not-authorized (fn [context] 
                           (info "get member global licenses for user" (:id (::user context)) "not authorized")
                           (to-json {:status "Not authorized"}))

  :handle-ok (fn [context] (let [licenses (db/global-licenses (::user context))]
                             (info "get member global licenses for user" (:id (::user context)) "ok")
        (map #(export-license % nil) licenses))))
  
(defresource member-publisher-name
  []
  :allowed-methods [:get :put :options]
  :available-media-types ["text/plain"]
  
  :authorized? (fn [context]
                 (authenticate-crossref-and context
                  (fn [user] {::user user})))
  
  :handle-not-authorized (fn [context]
                           (info "member publisher name for user" (:id (::user context)) "not authorized")
                           "Not authorized")
  
  :handle-ok (fn [context]
    (let [user (::user context)]
      (info "get member publisher name for user" (:id (::user context)) "ok")
      (or (:publisherName user) "")))
  
  :put! (fn [context] 
        (let [user (::user context)
              new-name (slurp (get-in context [:request :body]))]
               (db/set-publisher-name (::user context) new-name))
               (info "set member publisher name for user" (:id (::user context)) "ok")))

(defresource heartbeat
  []
  :allowed-methods [:get :options]
  :available-media-types ["text/plain"]
  :exists? (fn [context] (db/heartbeat))
  :handle-ok (fn [context]
               (info "heartbeat ok")
               "OK"))

(defresource licenses-api
  [token]
  :allowed-methods [:get :options]
  :available-media-types ["application/json"]

  :malformed? (fn [context]
                (let [crossref-user-token-deprecated (get (-> context :request :headers) "cr-prospect-publisher-token")
                      crossref-user-token-new (get (-> context :request :headers) "cr-clickthrough-publisher-token")
                      crossref-user-token (or crossref-user-token-new crossref-user-token-deprecated)]
                (if crossref-user-token 
                  [false {::crossref-user-token crossref-user-token}]
                  true)))
  
  :exists? (fn [context]
             (let [orcid-user (db/get-orcid-user-by-token token)
                     crossref-user (db/get-crossref-user-by-token (::crossref-user-token context))]
               (cond 
                 (nil? orcid-user) [false, "Can't find ORCID user"]
                 (nil? crossref-user) [false, "Can't find Publisher user"]
                  :else [true {::orcid-user orcid-user ::crossref-user crossref-user}])))
         
  :handle-ok (fn [context]
               (let [orcid-user (::orcid-user context)
                     licenses (db/get-licenses-for-researcher orcid-user)
                     exported-licenses (map transform-license-for-api licenses)]
                (info "License API OK for researcher token" token "member token" (::crossref-user-token context))
                (to-json {:result "ok"
                          :message "licenses"
                          :licenses exported-licenses}))))

(defresource js-config
  []
  :allowed-methods [:get]
  :available-media-types ["text/javascript"]
  :handle-ok (fn [context]
         
  (str "window.CROSS_REF = {
API_BASE: '" external/api-base "',
LOG_OUT: '" external/api-base "authenticate/logout',
LOG_IN_CROSS_REF: '" external/api-base "authenticate/crossref',
LOG_IN_ORCID: '" external/api-base "authenticate/orcid',
LICENSE_ANGULAR_PREFIX: '"  "',
                                   
LICENSE_STATUS_DRAFT: 0,
LICENSE_STATUS_PUBLISHED: 1,
LICENSE_STATUS_RETIRED: 2,
LICENSE_STATUS_DELETED: 3,
                                       
STATUS_REVIEW_NOT_REVIEWED : 'unreviewed',
STATUS_REVIEW_POSTPONED : 'postponed',
STATUS_REVIEW_REJECTED : 'rejected',
STATUS_REVIEW_ACCEPTED : 'accepted',
                                       
REVIEWERS_BASE_URL: '" external/reviewers-base-url "'};")))

; Middleware

(defn require-crossref-auth
  "Ensure that the user has logged in with CrossRef credentials."
  [request handler]
  (fn [request]
    (let [crossref-id (-> request :session session-crossref-id)
          user (when-not (nil? crossref-id) (db/get-or-create-crossref-user crossref-id))]
      (if (nil? user)
        (-> (response "Not authenticated with CrossRef ID") (status 401))
        (handler (assoc request :crossref-user user))))))

(defn require-orcid-auth
  "Ensure that the user has logged in with ORCID credentials."
  [request handler]
  (fn [request]
             (let [orcid-id (-> request :session session-orcid-id)
           orcid-user (when-not (nil? orcid-id) (db/get-or-create-orcid-user orcid-id))]
      (if (nil? orcid-id)
          ; The response should be a 401 with an URL to call as the body response.
          (-> (response external/orcid-full-auth-url) (status 401))
          (handler (assoc request :orcid-user orcid-user))))))

; Authentication 
(defn authenticate-orcid-test
  "Test for ORCID auth."
  [request]
  
  ; This is wrapped in require-orcid-auth which handles the check.
  (response "OK"))

(defn authenticate-crossref
  "Authenticate Publisher user using CrossRef auth."
  [request]
  (let [params (from-json (slurp (:body request)))
        username (:username params)
        password (:password params)
        auth-result (external/log-in-crossref username password)
        user (when
               (= auth-result true) (db/get-or-create-crossref-user username))
        new-session (cond 
          (and (= auth-result true) (not (nil? user))) (assoc (:session request) :session-crossref-id username)
          (= auth-result false) (:session request)
          (= auth-result :error) (:session request)
          :else (:session request))]
    
    (info "Authenticate member with CrossRef with username" username "result" auth-result)
    (-> (response (to-json {"result" auth-result}))
        (content-type "text/json")
        (status (if (= auth-result true) 200 401))
        (#(assoc % :session new-session)))))

(defn authenticate-crossref-test
  "Test for CrossRef auth."
  [request]
  ; This is wrapped in require-crossref-auth which handles the check.
  (response "OK"))

(defn authenticate-logout
  "Log out user, Publisher or Researcher."
  [request-s]
  (-> (response (to-json {:status "Logged out" :orcid-login-url external/orcid-full-auth-url}))
      (content-type "application/json")
      (status 401)
      (#(assoc % :session {}))))

(defn orcid-redirect
  [request]
  "The redirect endpoint for ORCID authentication."
  (let [params (:params request)
        code (:code params)
        orcid-id (external/fetch-authenticated-orcid code)
        orcid-user (when-not (nil? orcid-id)
                     (db/get-or-create-orcid-user orcid-id))
        new-session (cond 
          (and (not (nil? orcid-id)) (not (nil? orcid-user))) (assoc (:session request) session-orcid-id orcid-id)
          :else (:session request))]
  (if orcid-id    
    (-> (response external/orcid-redirect-html)
        (#(assoc % :session new-session))
        (status 200)
        )
    (-> (response external/orcid-redirect-html-bad)
        (#(assoc % :session new-session))
        (status 401)))))


; Misc

(defroutes auth-routes
  (GET "/api/authenticate/orcid" request (require-orcid-auth request authenticate-orcid-test))  
  (GET "/api/authenticate/crossref" request (require-crossref-auth request authenticate-crossref-test))
  (POST "/api/authenticate/crossref" request authenticate-crossref)
  (GET "/api/authenticate/logout" request authenticate-logout)
  
  ; See comment in external.
  (GET external/orcid-redirect-url-route [] orcid-redirect))

(defroutes api-routes
  (ANY ["/api/licenses/:token" :token #".*"] [token] (licenses-api token)))

(defroutes member-routes
  (ANY "/api/member/licenses" [] (member-licenses))
  (ANY ["/api/member/licenses/:id" :id #"[0-9]+"] [id] (member-license id))
  (ANY "/api/member/licenses/mine" [] (member-licenses))
  
  (ANY "/api/member/my/member/token" [] (publisher-token))
  (ANY "/api/member/me" [] (publisher-me))
  
  (ANY "/api/member/licenses/global" [] (member-global-licenses))
  (ANY ["/api/member/licenses/global/:id" :id #"[0-9]+"] [id] (member-global-license id))
  
  (ANY "/api/member/my/publisher/name" [] (member-publisher-name)))

; Prospector URLs are derecated but still respond.
(defroutes deprecated-prospector-routes
  (ANY ["/api/prospector/licenses/review/:uri" :uri #".*"] [uri] (license-review uri))
  (ANY ["/api/prospector/reviews/:state" :state #"unreviewed|accepted|rejected|postponed"] [state] (license-reviews state))
  (ANY "/api/prospector/my/prospector/token" [] (researcher-token)))

(defroutes researcher-routes
  (ANY ["/api/researcher/licenses/review/:uri" :uri #".*"] [uri] (license-review uri))
  (ANY ["/api/researcher/reviews/:state" :state #"unreviewed|accepted|rejected|postponed"] [state] (license-reviews state))
  (ANY "/api/researcher/my/researcher/token" [] (researcher-token))
  (ANY "/api/researcher/me" [] (researcher-me))
  (ANY ["/embed/uri/:uri" :uri #".*"] [uri] (embed-for-uri uri))
  (ANY ["/embed/doi/:doi" :doi #".*"] [doi] (embed-for-doi doi)))

(defroutes other-routes
  (ANY "/heartbeat" [] (heartbeat))
  (ANY "/api/heartbeat" [] (heartbeat))
  (ANY "/config/config.js" [] (js-config))

  (GET "/researchers" [] (redirect (p "/researchers/")))
  (GET "/publishers" [] (redirect (p "/publishers/")))
  
  (route/resources "/researchers/" {:root "researchers/app/index.html"})
  (route/resources "/publishers/" {:root "publishers/app/index.html"})
  
  (route/resources "/researchers/" {:root "researchers/app"})
  (route/resources "/publishers/" {:root "publishers/app"})

  (GET "" [] (redirect (p "/")))
  
  
  (route/resources "/" {:root "splash/index.html"})
  (route/resources "/" {:root "splash"})
  
  ; Redirect 'stable' URL for reviewing licenses to the Angular one. Allows swapping out Angular later.
  (GET ["/researchers/tc/:uri" :uri #".*"] [uri] (redirect (str (p "/researchers/#/tc/") uri))))


(def all-routes 
  (routes
    (-> api-routes)
    (-> auth-routes)
    (-> member-routes)
    (-> deprecated-prospector-routes)
    (-> researcher-routes)
    (-> other-routes)))

(def app
  (-> all-routes
      wrap-crossref-session
      wrap-orcid-session
      handler/site
      wrap-cors))

; Used by `lein deamon` and `lein run`.
(defn -main
  [& args]
      (info "Running on port" (:port config))
      (hs/run-server app {:port (:port config) :thread 10}))