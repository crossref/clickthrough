(ns clickthrough.entities
  (:require [clojure.set :refer [map-invert]])
  (:require [clickthrough.consts :refer :all])
  (:require [clojure.string :as string])
  )

(defn is-license-in-state
  "Check if license is in the given state given by symbol."
  [license state]
  (= (:status license) state))

; Fields that may be set in the database.
; In use, :id may also be required.
(def license-fields [:title :uri :description :createdBy :status :fullLicenseText])

; Fields that may not be changed unless in draft mode.
(def editable-license-fields (remove (partial = :status) license-fields))

(defn prefixed-fields
  "Prefix a seq of symbols Used for the fields clause in database query."
  [fields separator prefix] (map #(symbol (str prefix separator (name %))) fields))

(def prefix-license-fields (partial prefixed-fields license-fields "."))

; Internal field names -> exported field names.
(def export-license-fields
  {:status :status
   :description :description
   :uri :uri
   :fullLicenseText :full_text 
   :title :title
   :createdBy :created_by
   :id :id})

(defn swap-keys
  "Given an input map, map the keys according to another map."
  [key-mapping input]
  (apply hash-map (apply concat (map (fn [[k v]] [(or (k key-mapping) k) v]) input))))

; Transform a license to and from external JSON labels to internal symbol labels.
(defn export-license [license user] (let [new-keys (swap-keys export-license-fields license)
                          with-date (assoc new-keys :reviewed_at (str (:reviewed_at new-keys)))
                          
                          ; Set 'owned' field if the user is supplied.
                          user-id (:id user)
                          with-user (assoc with-date :owned (= (:created_by with-date) user-id))]
                      with-user))

(def export-license-review-fields
  {:status :license_status
   :description :description
   :uri :uri
   :fullLicenseText :full_text 
   :title :title
   :id :id
   :reviewedAt :reviewed_at})

(defn export-license-review [license user] (let [new-keys (swap-keys export-license-review-fields license)
                          with-date (assoc new-keys :reviewed_at (str (:reviewed_at new-keys)))
                          with-review (assoc with-date :status (get review-state-names (:review-status license)))
                          
                          
                          ]
                      with-review))

(def import-license (partial swap-keys (map-invert export-license-fields)))

(defn license-changed
  "Has the licence changed between between before and after?"
  [before after]
  (some false? (map #(= (get before %) (get after %)) editable-license-fields)))
  
(defn validate-license-state [license expected-user-id]
  (contains? license-states (:status license)))

; License Validation. 
(def license-validation-rules
  [
   ["Title must be supplied" (fn [license _] (not (string/blank? (:title license))))]
   ["Description must be supplied" (fn [license _] (not (string/blank? (:description license))))]
   ["URI must be supplied" (fn [license _] (not (string/blank? (:uri license))))]
   ["Full license text must be supplied" (fn [license _] (not (string/blank? (:fullLicenseText license))))]
   ["Status must be recognised" validate-license-state]
   ["Must be owned by logged-in user" (fn [license expected-user-id] (= (:createdBy license) expected-user-id))]
   ])

(defn apply-license-validation-rule
  [[message, rule] license expected-user-id]
  (let [result (apply rule [license expected-user-id])]
    (when-not result message)))

(defn validate-license
  "Validate license, return Seq of error messages."
  [license expected-user-id]
  (remove nil? (map #(apply-license-validation-rule % license expected-user-id) license-validation-rules)))

(defn validate-new-license
  "Validate a new license (status not required), return Seq of error messages."
  [license]
  (remove nil? (map #(apply-license-validation-rule % license) license-validation-rules)))

(defn validate-license-transition
  "Validate the the transition of state for a license. Return Seq of errors, empty Seq on success."
  [old-license new-license has-reviews]
    (let [old-state (:status old-license)
        new-state (:status new-license)
        state-transition-ok (contains? allowed-license-transitions [old-state new-state has-reviews])
        changed (license-changed old-license new-license)
        ]
    (and state-transition-ok (or (not changed) (= old-state draft-state)))))

(defn valid-review-state? [state]
  (contains? review-states state))

(defn generate-token
  "Generate a token of 4 × 4 byte digits in hex."
  []
  (string/join "-" (map #(apply str %) (partition 4 (map #(format "%02x" %) (take 16 (repeatedly #(rand-int 255))))))))

(defn transform-license-for-api
  "Take a researcher-license and transform it into the correct format for the API."
  [license]
  {:uri (:uri license)
   :status (get review-state-names (:review-status license))
   :reviewed_at (str (:reviewedAt license))})


(defn escape-text [text] (when text (-> text (string/replace "<" "&lt;") (string/replace ">" "&gt;"))))

(defn escape-license [license]
  (-> license
    (assoc :description (escape-text (:description license)))
    (assoc :title (escape-text (:title license)))
    (assoc :fullLicenseText (escape-text (:fullLicenseText license)))))