(ns clickthrough.database
    (:require [clickthrough.consts :refer :all])
    (:require [clickthrough.entities :refer :all])
    (:require [clickthrough.config :refer [config]])
    (:require [korma.core :refer [defentity entity-fields pk table select subselect where insert update values delete exec-raw set-fields fields sql-only join aggregate group with has-one has-many]])
    (:require [korma.db :refer [defdb mysql]])
    (:require [clj-time.core :as time]
              [clj-time.coerce :as coerce])
    (:require [clojure.string :as str]))

(defdb database (mysql {:db (config :agreements-db-name)
                       :user (config :agreements-db-username)
                       :host (or (config :agreements-db-host) "localhost")
                       :port (or (config :agreements-db-port) 3306)
                       :password (config :agreements-db-password)}))

(defentity orcid-user
  (pk :id)
  (table :orcidUser))

(defentity crossref-user
  (pk :id)
  (table :crossRefUser))

(defentity review
  (table :review))

(defentity license
  (pk :id)
  (table :license)
  (has-many review {:fk :license}))

(defentity crossref-user-access-token
  (pk :id)
  (table :crossRefUserAccessToken))

(defentity orcid-user-access-token
  (pk :id)
  (table :orcidUserAccessToken))

(defentity publisher-uses-license
  (pk :id)
  (table :publisherUsesLicense))

; Users and tokens

(defn heartbeat
  "Just do any old SQL query"
  []
  (try
    ; This will throw an exception if the DB's down.
    (select crossref-user (where {:id 0}))
    true
    (catch Exception _ false)))

(defn get-or-create-crossref-user
  "Get or create CrossRef user"
  [username]
  (let [user (select crossref-user (where {:username username}))]
    (if-not (empty? user)
      (first user)
      (let [new-user (insert crossref-user (values {:username username}))
            id (:GENERATED_KEY new-user)
            new-user (select crossref-user (where {:id id}))]
        (first new-user)))))

(defn get-crossref-user-by-token
  "Get a CrossRef user by their token"
  [token]
  (let [the-token (select crossref-user (where {:id [in
                    (subselect crossref-user-access-token
                      (fields :belongsTo)
                      (where {:value token :current true}))]}))]
    (first the-token)))

(defn get-orcid-user-by-token
  "Get an ORCID user by their token."
  [token]       
  (let [the-token (select orcid-user (where {:id [in
                    (subselect orcid-user-access-token
                      (fields :belongsTo)
                      (where {:value token :current true}))]}))]
    (first the-token)))

(defn insert-orcid-user-token
  "Insert an Orcid user token."
  [user token-value]
  (insert orcid-user-access-token
    (values {
      :belongsTo (:id user)
      :current true
      :value token-value})))

(defn get-orcid-user-token
  "Token from an ORCID user object"
  [user]
  (let [the-token (select orcid-user-access-token
    (where {:belongsTo (:id user) :current true})
    ; We only need the value. The retiredAt field isn't always going to be a valid datestamp so don't try and deserialise it.
    (fields :value))]
    (first the-token)))

(defn insert-crossref-user-token
  "Insert a CrossRef user token."
  [user token-value]
  (insert crossref-user-access-token
    (values {
      :belongsTo (:id user)
      :current true
      :value token-value
      })))

(defn get-crossref-user-token
  "Token from a CrossRef user object"
  [user]
  (let [the-token (select crossref-user-access-token (where {:belongsTo (:id user) :current true}))]
    (first the-token)))

(defn get-or-create-orcid-user
  "Get or create an ORCID user by ORCID id."
  [orcid]
  (let [user (select orcid-user (where {:orcid orcid}))]
    (if-not (empty? user)
      (first user)
      (let [new-user (insert orcid-user (values {:orcid orcid}))]
        (first new-user)))))

(defn revoke-crossref-user-token
  "Revoke a CrossRef user token."
  [user]
   (update crossref-user-access-token
    (set-fields {:current false  :retiredAt (coerce/to-sql-time (time/now))})
    (where {:belongsTo (:id user) :current true})))

(defn revoke-orcid-user-token
  "Revoke an ORCID users's token."
  [user]
  (update orcid-user-access-token
    (set-fields {:current false :retiredAt (coerce/to-sql-time (time/now))})
    (where {:belongsTo (:id user) :current true})))

(defn set-publisher-name
  "Set a Publisher's name"
  [user the-name]
  (update crossref-user
    (where {:id (:id user)})
    (set-fields {:publisherName the-name})))

; Licenses

(defn all-licenses
  "Fetch all licenses, optionally in state."
  [& [state]]
  (if (nil? state)
    (select license)
    (select license (where {:status state}))))

(defn global-licenses
  "Get all published global licenses and their review status for the user."
  [user]
  ; TODO apply fields in correct macro context with (conj (prefix-license-fields :license) [:publisherUsesLicense.id :use])
   (let [licenses
   (select license
    (fields [:publisherUsesLicense.id :use-id] :license.id :license.title :license.uri :license.fullLicenseText :license.description :license.createdBy :license.status)
    (where
      {:status published-state
       :createdBy [in (subselect crossref-user
                 (where {:isCrossRefStaff true})
                 (fields :id))]})
    (join "left outer" publisher-uses-license (and
      (= :publisherUsesLicense.crossRefUser (:id user))
      (= :publisherUsesLicense.license :license.id))))]
     (map #(assoc % :used (not (nil? (:use-id %)))) licenses)))

(defn is-license-used
  "Is the global license used by the user?"
  [license user]
  (< 0 (count
    (select publisher-uses-license
      (fields :id)
      (where {:crossRefUser (:id user) :license (:id license)})))))

(defn set-license-use
  "Set whether or not a publisher uses a license. Don't set twice."
  [license user should-use]
  (if should-use
    (when (not (is-license-used license user))
      (insert publisher-uses-license
        (values {:crossRefUser (:id user) :license (:id license)})))
    (delete publisher-uses-license
      (where {:crossRefUser (:id user) :license (:id license)}))))

(defn create-license
  "Create a license for a user. Return it or throw com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException."
  [the-license]
  (try
    (let [fields (-> the-license
             (select-keys license-fields))
          new-license-result (insert license (values fields))
          new-license-id (:GENERATED_KEY new-license-result)
          new-license (select license (where {:id new-license-id}))]
      (first new-license))
    (catch com.mysql.jdbc.exceptions.jdbc4.MySQLIntegrityConstraintViolationException e :duplicate-uri)))

(defn update-license
  "Update a license, if it belongs to the user and reutrn it."
  [id the-license the-crossref-user]

  (update license
    (set-fields (select-keys the-license license-fields))
    (where {
      :createdBy (:id the-crossref-user)
      :id id}))
    the-license)

(defn get-licenses
  "Get licenses that belong to the crossref-user"
  [the-crossref-user]
  (select license (where {:createdBy (:id the-crossref-user)})))

(defn- transform-license-with-reviews
  "Transform a license result with the aggregated review structure and return the values as [:accepts, :rejects, :postpones on the license object]."
  [license-result]
  (let [reviews (or (:review license-result) [])
        num-postpones (or (:count (first (filter #(= (:status %) review-postponed-state) reviews))) 0)
        num-accepts (or (:count (first (filter #(= (:status %) review-accepted-state) reviews))) 0)
        num-rejects (or (:count (first (filter #(= (:status %) review-rejected-state) reviews))) 0)]
      (-> license-result (dissoc :review)  (assoc :postpones num-postpones :accepts num-accepts :rejects num-rejects))))

(defn get-license-for-member
  "Get license by id, including reviews."
  [id]
  (transform-license-with-reviews (first (select license
          (where {:id id})
    (with review (fields :status) (aggregate (count :*) :count :status))))))

(defn- transform-license-with-review-status
  "Transform a license result with the aggregated review structure and return with :review-status as [:none :accepted :rejected :postponed]."
  [license-result]
  (let [; There will only be one review, or none.
        review (first (or (:review license-result) []))
        status (:status review)
        reviewed-at (:reviewedAt review)
        status (cond 
                 (nil? review) review-unreviewed-state
                 (contains? review-states status) status
                 ; This shouldn't happen, so handle that as unreviewed.
                 :else review-unreviewed-state)]
      (-> license-result (dissoc :review) (assoc :review-status status) (assoc :reviewedAt reviewed-at))))

(defn get-licenses-for-researcher
  "Get all licenses for a Researcher along with the review status of each."
  [the-orcid-user & criteria]
  {:pre [the-orcid-user (:id the-orcid-user)]}
  (let [select-criteria (or (first criteria) {})
        user-id (:id the-orcid-user)
        licenses (select license
        (where select-criteria)
        (with review
          (fields :status :reviewedAt)
          (aggregate (count :*) :count :status)
          (where {:orcidUser (:id the-orcid-user)})))
        transformed (map transform-license-with-review-status licenses)]
    transformed))

(defn get-licenses-for-no-researcher
  "Get all licenses for a not-logged-in Researcher."
  [& criteria]
  (let [select-criteria (or (first criteria) {})
        licenses (select license
        (where select-criteria))
        transformed (map transform-license-with-review-status licenses)]
    transformed))

; Researchers don't get to see un-published licenses.
(defn get-all-licenses-for-researcher [the-orcid-user] (get-licenses-for-researcher the-orcid-user {:status published-state}))
(defn get-license-for-researcher-by-id [the-orcid-user id] (first (get-licenses-for-researcher the-orcid-user {:id id :status published-state})))
(defn get-license-for-researcher-by-url [the-orcid-user url] (first (get-licenses-for-researcher the-orcid-user {:uri url :status published-state})))
  
(defn get-license-for-no-researcher-by-url [url] (first (get-licenses-for-no-researcher {:uri url :status published-state})))
  
(defn get-license
  "Get license by id"
  [id]
  (first (select license (where {:id id}))))

(defn get-license-by-url
  "Get license by URL"
  [uri]
  (first (select license (where {:uri uri}))))

(defn perform-review
  "Review the license for the given Researcher."
  [license status user]
  (let [license-id (:id license)
        user-id (:id user)]
    (delete review (where {:license license-id :orcidUser user-id}))
    (insert review (values {:license license-id :orcidUser user-id :status status}))))