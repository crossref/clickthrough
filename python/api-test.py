import random
import unittest
import json
import requests
import time
import os
import mysql.connector

# Run this with api-test.sh. This will start the server and then run this file.
# Load the same config that the API should be running on (thanks to api-test.sh)

# TODO
# - add test for 'owned by'

runs = [True]

with open("./config.apitest.json") as f:
    config = json.load(f)

base = config["SERVE_ON"]

LICENSE_STATE_DRAFT     = 0
LICENSE_STATE_PUBLISHED = 1
LICENSE_STATE_RETIRED   = 2
LICENSE_STATE_DELETED   = 3

STATUS_REVIEW_NOT_REVIEWED = "unreviewed" #0
STATUS_REVIEW_POSTPONED    = "postponed" #1
STATUS_REVIEW_REJECTED     = "rejected" #2
STATUS_REVIEW_ACCEPTED     = "accepted" #3

# These are copied from handlers.go
ROUTE_AUTHENTICATE_ORCID     = base + "/authenticate/orcid"
ROUTE_AUTHENTICATE_CROSS_REF = base + "/authenticate/crossref"
ROUTE_AUTHENTICATE_LOGOUT    = base + "/authenticate/logout"
ROUTE_ALL_LICENSES           = base + "/member/licenses"
ROUTE_MY_LICENSES            = base + "/member/licenses/mine"
ROUTE_GLOBAL_LICENSES        = base + "/member/licenses/global"
ROUTE_GLOBAL_LICENSE         = base + "/member/licenses/global/"
ROUTE_LICENSE                = base + "/member/licenses/"
ROUTE_HEARTBEAT              = base + "/heartbeat"

ROUTE_MEMBER_TOKEN           = base + "/member/my/member/token"
ROUTE_RESEARCHER_TOKEN       = base + "/researcher/my/researcher/token"

ROUTE_MEMBER_NAME            = base + "/member/my/publisher/name"

ROUTE_REVIEWS_UNREVIEWED     = base + "/researcher/reviews/unreviewed"
ROUTE_REVIEWS_ACCEPTED       = base + "/researcher/reviews/accepted"
ROUTE_REVIEWS_REJECTED       = base + "/researcher/reviews/rejected"
ROUTE_REVIEWS_POSTPONED      = base + "/researcher/reviews/postponed"

ROUTE_REVIEW_LICENSE         = base + "/researcher/licenses/review/"

# ROUTE_CLEAR_DB               = base + "/test/cleardb"

ROUTE_API_LICENSES           = base + "/licenses/"

# Two CrossRef users.
CROSS_REF_LOGIN_CREDS = '{"username":"testuser","password":"testpassword"}'
CROSS_REF_LOGIN_CREDS_2 = '{"username":"testuser_2","password":"testpassword_2"}'
CROSS_REF_BAD_LOGIN_CREDS = '{"username":"jim","password":"1234"}'

# ORCID callback URL. Calling this jumps to the last stage of the mocked-out OAUTH sequence.
ORCID_LOGIN_CALLBACK = base + "/auth/orcid/callback?code=xgTvJNX"

# The API header field is changing. API should support both.
PUBLISHER_TOKEN_HEADER_FIELDS = ["CR-Prospect-Publisher-Token", "CR-Clickthrough-Publisher-Token"]

# Wait for API to come up.
for x in range(1, 10):
    try:
        requests.get(base).status_code
        break
    except requests.exceptions.ConnectionError:
        time.sleep(0.5)

class CrossRefBits(unittest.TestCase):
    """
    Common base class for tests which includes authentication, database teardown between tests.
    """

    def setUp(self):
        # Every test, clear everything in the database.
        self.truncate_database()

        # Get an authenticated cookie for a CrossRef session.
        login_request = requests.post(ROUTE_AUTHENTICATE_CROSS_REF, data=CROSS_REF_LOGIN_CREDS, headers={"Content-Type": "application/json"})
        self.assertEqual(login_request.status_code, 200)
        self.crossref_cookies = login_request.cookies

        # For some tests we need another CrossRef user.
        login_request_2 = requests.post(ROUTE_AUTHENTICATE_CROSS_REF, data=CROSS_REF_LOGIN_CREDS_2, headers={"Content-Type": "application/json"})
        self.assertEqual(login_request_2.status_code, 200)
        self.crossref_cookies_2 = login_request_2.cookies

        # Cookie for an ORCID session.
        login_request = requests.get(ORCID_LOGIN_CALLBACK)
        self.assertEqual(login_request.status_code, 200)
        self.orcid_cookies = login_request.cookies

    def make_staff_user(self, username):
        """
        Make a given user staff.
        """
        
        db = mysql.connector.connect(
            host=config["PY_SQL_HOST"],
            database=config["PY_SQL_DATABASE"],
            user=config["PY_SQL_USER"],
            password=config["PY_SQL_PASSWORD"],
            port=config["PY_SQL_PORT"],
        )
        
        cursor = db.cursor()
        r = cursor.execute("UPDATE crossRefUser SET isCrossRefStaff = TRUE WHERE username = %s;", (username,))
        cursor.close()

        db.commit()
        db.close()

    def truncate_database(self):
        db = mysql.connector.connect(
            host=config["PY_SQL_HOST"],
            database=config["PY_SQL_DATABASE"],
            user=config["PY_SQL_USER"],
            password=config["PY_SQL_PASSWORD"],
            port=config["PY_SQL_PORT"],
        )
        
        cursor = db.cursor()

        r = cursor.execute("TRUNCATE TABLE orcidUser")
        r = cursor.execute("TRUNCATE TABLE crossRefUser")
        r = cursor.execute("TRUNCATE TABLE license")
        r = cursor.execute("TRUNCATE TABLE review")
        r = cursor.execute("TRUNCATE TABLE crossRefUserAccessToken")
        r = cursor.execute("TRUNCATE TABLE orcidUserAccessToken")
        r = cursor.execute("TRUNCATE TABLE publisherUsesLicense")

        cursor.close()

        db.commit()
        db.close()


    def reviews_in_state(self, state):
        """
        Fetch all reviews in given state as the ORCID user.
        """
        if state == STATUS_REVIEW_NOT_REVIEWED:
            url = ROUTE_REVIEWS_UNREVIEWED
        elif state == STATUS_REVIEW_ACCEPTED:
            url = ROUTE_REVIEWS_ACCEPTED
        elif state == STATUS_REVIEW_REJECTED:
            url = ROUTE_REVIEWS_REJECTED
        elif state == STATUS_REVIEW_POSTPONED:
            url = ROUTE_REVIEWS_POSTPONED
        else:
            raise Exception("unrecognised state")

        for_review = requests.get(url, cookies=self.orcid_cookies)
        self.assertTrue(for_review.status_code, 200)
        reviews = json.loads(for_review.text)

        return reviews

    def get_my_licenses(self, cookies=None, expected_status_code=200):
        """
        Fetch all of the given crossref user's licenses.
        """
        cookies = cookies or self.crossref_cookies


        licenses = requests.get(ROUTE_MY_LICENSES, cookies=cookies)

        self.assertEqual(licenses.status_code, expected_status_code)

        return json.loads(licenses.text)

    def create_license_in_state(self, uri, state,
        title="license title",
        full_text="full text",
        description="description",
        cookies=None,
        ):

        """
        Create license in given state (or None for default state) as the CrossRef user,
        return URL of the resource and the new content.
        """

        cookies = cookies or self.crossref_cookies

        # Create as a draft first.
        license_input = {
            "title": title,
            "full_text": full_text,
            "uri": uri,
            "description": description,
            "status": LICENSE_STATE_DRAFT,
        }
        
        post_request = requests.post(ROUTE_ALL_LICENSES, cookies=cookies, data=json.dumps(license_input), headers={"Content-Type": "application/json"})
        self.assertEqual(post_request.status_code, 201)

        license = json.loads(post_request.text)
        
        url = ROUTE_ALL_LICENSES + "/" + repr(license['id'])

        # None means don't sent a state.
        if state is None:
            license_input.pop('status')

        # Change to new state.
        elif state == LICENSE_STATE_DRAFT:
            # Already a draft.
            pass
        elif state == LICENSE_STATE_RETIRED:
            # Need to go via Published first.

            license_input['status'] = LICENSE_STATE_PUBLISHED
            license_response = requests.put(url, cookies=cookies, data=json.dumps(license_input), headers={"Content-Type": "application/json"})

            self.assertEqual(license_response.status_code, 201)

            license_input['status'] = LICENSE_STATE_RETIRED
            license_response = requests.put(url, cookies=cookies, data=json.dumps(license_input), headers={"Content-Type": "application/json"})

            self.assertEqual(license_response.status_code, 201)
        else:
            license_input['status'] = state

            license_response = requests.put(url, cookies=cookies, data=json.dumps(license_input), headers={"Content-Type": "application/json"})            

            self.assertEqual(license_response.status_code, 201)
        
        # Now re-fetch.
        fetched_response = requests.get(url, cookies=cookies)

        self.assertEqual(fetched_response.status_code, 200)
        fetched_json = json.loads(fetched_response.text)
        
        return url, fetched_json

    def update_license(self, license, expected_status_code=201, should_return=True):
        """
        Make an update to an existing license.
        """
        
        url = "%s/%d" % (ROUTE_ALL_LICENSES, license['id'])

        license_response = requests.put(url, cookies=self.crossref_cookies, data=json.dumps(license), headers={"Content-Type": "application/json"})

        self.assertEqual(license_response.status_code, expected_status_code)

        if should_return:

            # Fetch again.
            fetched_response = requests.get(url, cookies=self.crossref_cookies)
            self.assertEqual(fetched_response.status_code, 200)
            fetched_json = json.loads(fetched_response.text)
        
            return fetched_json
        else:
            return None

    def refetch_license(self, license, expected_status_code=200, should_return=True):
        url = ROUTE_ALL_LICENSES + "/" + repr(license['id'])
        license_response = requests.get(url, cookies=self.crossref_cookies)
        self.assertEqual(license_response.status_code, expected_status_code)

        if should_return:
            return json.loads(license_response.text)



    def review_license(self, license, state, expected_status_code=201):
        """
        Review a license as the ORCID user.
        """

        license = license.copy()
        full_url = ROUTE_REVIEW_LICENSE + license['uri']

        if state == STATUS_REVIEW_ACCEPTED:
            state_name = "accepted"
        elif state == STATUS_REVIEW_REJECTED:
            state_name = "rejected"
        elif state == STATUS_REVIEW_POSTPONED:
            state_name = "postponed"
        else:
            raise Exception("bad review state %d" % state)

        license["status"] = state_name
        
        review_request = requests.put(full_url, cookies=self.orcid_cookies, data=json.dumps(license), headers={"Content-Type": "application/json"})

        self.assertEqual(review_request.status_code, expected_status_code)

    def get_researcher_token(self):
        req = requests.get(ROUTE_RESEARCHER_TOKEN, cookies=self.orcid_cookies)
        self.assertEqual(req.status_code, 200)

        token = req.text        
        self.assertTrue(len(token) > 0)
        return token

    def get_member_token(self):
        req = requests.get(ROUTE_MEMBER_TOKEN, cookies=self.crossref_cookies)
    
        self.assertEqual(req.status_code, 200)

        token = req.text        

        self.assertTrue(len(token) > 0)
        return token

    def api_fetch(self, token_header):
        researcher_token = self.get_researcher_token()
        publisher_token = self.get_member_token()

        req = requests.get(ROUTE_API_LICENSES + researcher_token, headers = {token_header: publisher_token})
        self.assertEqual(req.status_code, 200)

        return json.loads(req.text)


class TestBasic(CrossRefBits):
    """
    Tests for fundamental things.
    """
    
    def test_heartbeat(self):
        """
        Heartbeat should respond.
        """
        for _ in runs:
            self.assertEqual(requests.get(base + "/heartbeat").status_code, 200)

    def Xtest_CORS(self):
        """
        Relevant CORS heders should be set.
        Access-Control-Allow-Methods should contain at least GET, POST, PUT, OPTIONS
        Access-Control-Allow-Headers should contain at least Access-Control-Allow-Origin, Access-Control-Allow-Credentials
        Access-Control-Allow-Credentials should be true
        Access-Control-Allow-Origin should be the same Origin header as sent.

        Test only on the home URL.
        """
        for _ in runs:
            request = requests.get(base + "/", headers={"Origin": "http://test.crossref.com"})

            # Headers must be present.
            self.assertTrue('access-control-allow-methods' in request.headers)
            self.assertTrue('access-control-allow-headers' in request.headers)
            self.assertTrue('access-control-allow-credentials' in request.headers)
            self.assertTrue('access-control-allow-origin' in request.headers)

            # Given methods should be included.
            allow_methods = [method.lower().strip() for method in request.headers['access-control-allow-methods'].split(",")]
            self.assertTrue("get" in allow_methods)
            self.assertTrue("post" in allow_methods)
            self.assertTrue("put" in allow_methods)
            self.assertTrue("options" in allow_methods)

            # Given headers should be allowed.
            allow_headers = [header.lower().strip() for header in request.headers['access-control-allow-headers'].split(",")]
            self.assertTrue("access-control-allow-origin" in allow_headers)
            self.assertTrue("access-control-allow-credentials" in allow_headers)

            # Access-Control-Allow-Credentials should be true.
            self.assertEqual(request.headers["access-control-allow-credentials"], "true")

            # Access-Control-Allow-Origin should be what was sent.
            self.assertEqual(request.headers["access-control-allow-origin"], "http://test.crossref.com")
        

class TestAuthentication(CrossRefBits):
    """
    Tests for the authentication bits.
    """

    def setUp(self):

        # Override base setup beacuse it authenticates.
        pass

    def test_crossref_should_401(self):
        """
        CrossRef API should 401 if not authenticated.
        """
        for _ in runs:
            self.assertEqual(requests.get(base + "/authenticate/crossref").status_code, 401)

    def test_orcid_should_redirect(self):
        """
        ORCID API should 401 if not authenticated.
        """
        for _ in runs:
            self.assertEqual(requests.get(base + "/authenticate/orcid").status_code, 401)

    def test_crossref_bad_auth(self):
        """
        Authenticate with bad details gives a 401.
        """

        # 401 to start with.
        self.assertEqual(requests.get(ROUTE_AUTHENTICATE_CROSS_REF).status_code, 401)
        
        # Now we log in.
        login_request = requests.post(ROUTE_AUTHENTICATE_CROSS_REF, data=CROSS_REF_BAD_LOGIN_CREDS, headers={"Content-Type": "application/json"})
        
        # Should not have authenticated.
        self.assertEqual(login_request.status_code, 401)

        # Remains 401.
        self.assertEqual(requests.get(ROUTE_AUTHENTICATE_CROSS_REF).status_code, 401)

    def test_crossref_roundtrip(self):
        """
        An initial call to the CrossRef auth endpoint should return 401.
        When incorrect login details are posted, should return 401.
        When correct login details are posted, should return 200 and set cookie.
        When subsequent access with authenticated cookie, should return 200.
        When logout is called, subsequent access with cookie should return 401.
        """
        for _ in runs:
            
            # 401 to start with.
            self.assertEqual(requests.get(ROUTE_AUTHENTICATE_CROSS_REF).status_code, 401)
            
            # Now we log in.
            login_request = requests.post(ROUTE_AUTHENTICATE_CROSS_REF, data=CROSS_REF_LOGIN_CREDS, headers={"Content-Type": "application/json"})

            # Should have authenticated!
            self.assertEqual(login_request.status_code, 200)

            # Now we GET with the logged-in cookie.
            # We should remain logged in.
            self.assertEqual(requests.get(ROUTE_AUTHENTICATE_CROSS_REF, cookies=login_request.cookies).status_code, 200)

            # A couple of times.
            self.assertEqual(requests.get(ROUTE_AUTHENTICATE_CROSS_REF, cookies=login_request.cookies).status_code, 200)

            # Now we log out.
            # Response should be 401.
            logout_request = requests.get(base + "/authenticate/logout")
            self.assertEqual(logout_request.status_code, 401)

            # Now we GET with the cookie.
            # We shouldn't be allowed.
            self.assertEqual(requests.get(ROUTE_AUTHENTICATE_CROSS_REF, cookies=logout_request.cookies).status_code, 401)


    def test_orcid_roundtrip(self):
        """
        An initial call to the ORCID auth endpoint should return 401 and the body should contain a token.
        """
        for _ in runs:

            # First call returns not auth'd.
            request = requests.get(ROUTE_AUTHENTICATE_ORCID)
            self.assertEqual(request.status_code, 401)

            # Should get a URL back from it.
            authenticate_url = request.text

            # Visit the URL in question. 
            # Normally this involves entering usernames and passwords and then a redirect.
            # The auth server fakes this and jumps straight to the redirect.
            auth_response = requests.get(authenticate_url)

            # The response will include a redirect back to the license server with a token, the request will follow it.
            # The end result is a 200 back from the license server. And some cookies.
            self.assertEqual(auth_response.status_code, 200)
            cookies = auth_response.cookies

            # Now we should be authenticated.
            request = requests.get(ROUTE_AUTHENTICATE_ORCID, cookies=cookies)
            self.assertEqual(request.status_code, 200)


class TestLicensesMember(CrossRefBits):
    """
    Tests for CRUD of APIs from the Member's (publishers, etc) point of view.

    Status machine for License:

    States:
    draft, deleted, published, accepted, retired

    draft -> published
    draft -> deleted
    published -> accepted
    published -> draft
    published -> retired
    accepted -> retired

    Deleted:
    Deleted. Still exists in system. Has never been seen by anyone but its owner.

    Draft:
    Newly created. Every draft as stored in the service is valid, however.
    May be published or deleted.

    Published:
    Public and available for Researchers to review and accept.
    As soon as this is accepted by one Researcher, it becomes Accepted.
    May be accepted or re-Draft-ed.

    Accepted:
    Published and has been Accepted by at least one Researcher.
    May be Accepted by other Researchers.
    May be retired.

    Retired:
    Published and has been Accepted by at least one Researcher. 
    May not be accepted by new Researchers.
    Existing 'Accepts' still exist.

    Note on IDs:
    From the Researcher's ID, the URI is the identity of the object. However, the status of the license is mutable for the duration of its Draft status.
    So for the purpose of the Member API, the PK is a surrogate ID.
    Once a license has been published and it appears to Researchers, it is frozen and the URI is used as the PK.
    """

    def test_can_get_global_licenses(self):
        """
        Licenses created by a staff user are Global.
        Licenses created by a not-staff user are not Global.
        The Global licenses endpoint should return only Global licenses.
        The Global licenses endpoint should return only Published licenses.
        """
        for _ in runs:

            # User 2 is staff.
            self.make_staff_user("testuser_2")

            # Create six licenses by a Staff user, two in each state.
            _, global_draft_1 = self.create_license_in_state(
                "http://global_draft_1",
                LICENSE_STATE_DRAFT,
                title="global_draft_1",
                cookies=self.crossref_cookies_2,
            )

            _, global_draft_2 = self.create_license_in_state(
                "http://global_draft_2",
                LICENSE_STATE_DRAFT,
                title="global_draft_2",
                cookies=self.crossref_cookies_2,
            )

            _, global_published_1 = self.create_license_in_state(
                "http://global_published_1",
                LICENSE_STATE_PUBLISHED,
                title="global_published_1",
                cookies=self.crossref_cookies_2,
            )

            _, global_published_2 = self.create_license_in_state(
                "http://global_published_2",
                LICENSE_STATE_PUBLISHED,
                title="global_published_2",
                cookies=self.crossref_cookies_2,
            )

            _, global_retired_1 = self.create_license_in_state(
                "http://global_retired_1",
                LICENSE_STATE_RETIRED,
                title="global_retired_1",
                cookies=self.crossref_cookies_2,
            )

            _, global_retired_2 = self.create_license_in_state(
                "http://global_retired_2",
                LICENSE_STATE_RETIRED,
                title="global_retired_2",
                cookies=self.crossref_cookies_2,
            )

            _, global_deleted_1 = self.create_license_in_state(
                "http://global_deleted_1",
                LICENSE_STATE_DELETED,
                title="global_deleted_1",
                cookies=self.crossref_cookies_2,
            )

            _, global_deleted_2 = self.create_license_in_state(
                "http://global_deleted_2",
                LICENSE_STATE_DELETED,
                title="global_deleted_2",
                cookies=self.crossref_cookies_2,
            )

            global_for_staff = requests.get(ROUTE_GLOBAL_LICENSES, cookies=self.crossref_cookies_2)
            self.assertEqual(global_for_staff.status_code, 200)
            global_for_staff_response = json.loads(global_for_staff.text)

            global_for_normal = requests.get(ROUTE_GLOBAL_LICENSES, cookies=self.crossref_cookies)
            self.assertEqual(global_for_normal.status_code, 200)
            global_for_normal_response = json.loads(global_for_normal.text)

            # For both users the list of global licenses should contain only global_published_1 and global_published_2.

            self.assertEqual(len(global_for_staff_response), 2)
            for uri in ["http://global_published_1", "http://global_published_2"]:
                self.assertEqual(len(filter(lambda x:x['uri'] == uri, global_for_staff_response)), 1)

            self.assertEqual(len(global_for_staff_response), 2)
            for uri in ["http://global_published_1", "http://global_published_2"]:
                self.assertEqual(len(filter(lambda x:x['uri'] == uri, global_for_normal_response)), 1)

            # They should both be 'unused' because they're newly created.
            self.assertEqual(len(filter(lambda x:x['used'] == False, global_for_staff_response)), 2)
            self.assertEqual(len(filter(lambda x:x['used'] == False, global_for_normal_response)), 2)

            # Now both users 'use' a different one of them.
            user_1_will_use = global_for_normal_response[0]
            user_1_will_not_use = global_for_normal_response[1]

            user_2_will_use = global_for_staff_response[1]
            user_2_will_not_use = global_for_staff_response[0]

            user_1_uses = json.loads(requests.get(ROUTE_GLOBAL_LICENSE + repr(user_1_will_use['id']), cookies=self.crossref_cookies).text)
            user_2_uses = json.loads(requests.get(ROUTE_GLOBAL_LICENSE + repr(user_2_will_use['id']), cookies=self.crossref_cookies_2).text)

            # Should also be repored as unused from this endpoint.
            self.assertFalse(user_1_uses['used'])
            self.assertFalse(user_2_uses['used'])

            # Now change and update.
            user_1_uses['used'] = True
            user_2_uses['used'] = True

            x = requests.put(ROUTE_GLOBAL_LICENSE + repr(user_1_will_use['id']), cookies=self.crossref_cookies, data=json.dumps(user_1_uses), headers={"Content-Type": "application/json"})
            self.assertEqual(x.status_code, 201)
            y = requests.put(ROUTE_GLOBAL_LICENSE + repr(user_2_will_use['id']), cookies=self.crossref_cookies_2, data=json.dumps(user_2_uses), headers={"Content-Type": "application/json"})
            self.assertEqual(y.status_code, 201)

            # And re-fetch.
            user_1_uses = json.loads(requests.get(ROUTE_GLOBAL_LICENSE + repr(user_1_will_use['id']), cookies=self.crossref_cookies).text)
            user_2_uses = json.loads(requests.get(ROUTE_GLOBAL_LICENSE + repr(user_2_will_use['id']), cookies=self.crossref_cookies_2).text)

            # Now should be true.
            self.assertTrue(user_1_uses['used'])
            self.assertTrue(user_2_uses['used'])

            # Now check the 'all global' endpoint. New values should be reflected.
            global_for_staff = requests.get(ROUTE_GLOBAL_LICENSES, cookies=self.crossref_cookies_2)
            global_for_staff_response = json.loads(global_for_staff.text)

            for uri, used in [(user_2_will_use['uri'], True), (user_2_will_not_use['uri'], False)]:
                self.assertEqual(len(filter(lambda x:x['uri'] == uri and x['used'] == used, global_for_staff_response)), 1)

            global_for_normal = requests.get(ROUTE_GLOBAL_LICENSES, cookies=self.crossref_cookies)
            global_for_normal_response = json.loads(global_for_normal.text)

            for uri, used in [(user_1_will_use['uri'], True), (user_1_will_not_use['uri'], False)]:
                self.assertEqual(len(filter(lambda x:x['uri'] == uri and x['used'] == used, global_for_normal_response)), 1)

    def test_can_list_my_licenses(self):
        """
        It should be possible to list Member's licenses.
        More assertions below.
        """
        for _ in runs:
            
            licenses_request = requests.get(ROUTE_MY_LICENSES, cookies=self.crossref_cookies)
            
            # We can fetch without error.
            self.assertTrue(licenses_request.status_code, 200)

            # And something comes back (even empty is "[]").
            self.assertNotEqual(licenses_request.text, "")

    def test_can_create_license(self):
        """
        An authenticated Member can create licenses by POSTing.
        """

        for _ in runs:
            
            # Should create OK
            url, returned = self.create_license_in_state(
                "http://example.com", 
                None,
                title="User one license",
                full_text="full text",
                description="description",
            )
            
            # Should get the same fields back.
            self.assertEqual(returned['uri'], "http://example.com")
            self.assertEqual(returned['title'], "User one license")
            self.assertEqual(returned['full_text'], "full text")
            self.assertEqual(returned['description'], "description")


    def test_license_validation_creation(self):
        """
        A new License can't be created unless it passes validation.
        A non-validating POST should receive a 400.
        """
        for _ in runs:
            url_counter = 0

            license_input = {
                "title": "title",
                "full_text": "fulltext",
                "uri": None,
                "description": "description"
            }

            # For each field, submit with that field missing.
            # URL is immutable.
            for field_name in ["title", "full_text", "description"]:

                # Unique URL required each time.
                url_counter += 1
                url = "http://test_license_validation_creation-%d.com" % url_counter

                bad_copy = license_input.copy()
                bad_copy["uri"] = url
                bad_copy.pop(field_name)
                
                post_request = requests.post(ROUTE_ALL_LICENSES, cookies=self.crossref_cookies, data=json.dumps(bad_copy), headers={"Content-Type": "application/json"})

                self.assertEqual(post_request.status_code, 400)

            # Now, for sanity, create it with the real proper data to prove it wasn't something else wrong.
            good_copy = license_input.copy()
            url_counter += 1
            good_copy["uri"] = "http://test_license_validation_creation-good-%d.com" % url_counter

            post_request = requests.post(ROUTE_ALL_LICENSES, cookies=self.crossref_cookies, data=json.dumps(good_copy), headers={"Content-Type": "application/json"})

            self.assertEqual(post_request.status_code, 201)

            license_object = json.loads(post_request.text)

            # All fields should have been send and returned.
            for field in good_copy.keys():
                self.assertTrue(license_object[field] == good_copy[field])

            license_url = ROUTE_ALL_LICENSES + "/" + repr(license_object['id'])

            # And GET it to check that it updated.
            license_response = requests.get(license_url, cookies=self.crossref_cookies)
            license_object = json.loads(license_response.text)

            for field in good_copy.keys():
                self.assertTrue(license_object[field] == good_copy[field])


    def test_cant_create_other_user_id(self):
        """
        A License can't be created with a spoofed user id another user.
        """
        for _ in runs:
            # Create a license belonging to user 1.

            license_1_input = {
                "title": "title",
                "full_text": "fulltext",
                "uri": "http://example3.com",
                "description": "description"
            }

            post_1_request = requests.post(ROUTE_ALL_LICENSES, cookies=self.crossref_cookies, data=json.dumps(license_1_input), headers={"Content-Type": "application/json"})

            self.assertEqual(post_1_request.status_code, 201)

            license_1_object = json.loads(post_1_request.text)
            license_1_url = ROUTE_ALL_LICENSES + "/" + repr(license_1_object['id'])

            # Created with user1's id.
            self.assertTrue("created_by" in license_1_object)
            user_id_1 = license_1_object['created_by']

            # created_by cannot be set if it's not the owner's id.
            self.assertEqual(requests.put(license_1_url, cookies=self.crossref_cookies, data=json.dumps(license_1_object), headers={"Content-Type": "application/json"}).status_code, 201)

            # But if it's different then that's an error.
            # Should get 'unauthorized'.
            self.assertEqual(requests.put(license_1_url, cookies=self.crossref_cookies_2, data=json.dumps(license_1_object), headers={"Content-Type": "application/json"}).status_code, 403)

            # Now create a license for user 2.

            license_2_input = {
                "title": "title_2",
                "full_text": "fulltext",
                "uri": "http://example4.com",
                "description": "description"
            }

            post_2_request = requests.post(ROUTE_ALL_LICENSES, cookies=self.crossref_cookies_2, data=json.dumps(license_2_input), headers={"Content-Type": "application/json"})
            self.assertEqual(post_2_request.status_code, 201)

            license_2_object = json.loads(post_2_request.text)
            license_2_url = ROUTE_ALL_LICENSES + "/" + repr(license_2_object['id'])

            # Created with user1's id.
            user_id_2 = license_2_object['created_by']

            # Assert sanity.
            self.assertNotEqual(user_id_1, user_id_2)


    def test_license_validation_update(self):
        """
        An existing License can't be updated unless it passes validation.
        A non-validating PUT should receive a 400.
        """
        for _ in runs:
            # First create one.
            license_input = {
                "title": "title",
                "full_text": "fulltext",
                "uri": "http://test_license_validation_update.com",
                "description": "description"
            }

            post_request = requests.post(ROUTE_ALL_LICENSES, cookies=self.crossref_cookies, data=json.dumps(license_input), headers={"Content-Type": "application/json"})

            self.assertEqual(post_request.status_code, 201)
            license_response = json.loads(post_request.text)

            # Should now live at this URL.
            license_url = ROUTE_ALL_LICENSES + "/" + repr(license_response['id'])

            license_input_updated = {
                "title": "titleUPDATED",
                "full_text": "fulltextUPDATED",
                "uri": "http://test_license_validation_updateUPDATED.com",
                "description": "descriptionUPDATED",
                "status": LICENSE_STATE_DRAFT,
                "created_by": license_response['created_by'],
            }

            # Now we try to update it, each time with a different field missing.
            for field_name in license_input_updated.keys():
                bad_copy = license_input_updated.copy()

                # Set blank rather than not including it.
                # Fields that are missing are back-filled from existing values by the API.
                bad_copy[field_name] = ""

                post_request = requests.put(license_url, cookies=self.crossref_cookies, data=json.dumps(bad_copy), headers={"Content-Type": "application/json"})
        
                # Should get 'conflict'.
                self.assertEqual(post_request.status_code, 409)

                # Re-fetching should return the original without changes.
                license_response = requests.get(license_url, cookies=self.crossref_cookies)
                license_object = json.loads(license_response.text)

                for field in license_input.keys():
                    self.assertTrue(license_object[field] == license_input[field])

            # Now, for sanity, update it with the real proper data to prove it wasn't something else wrong.
            put_request = requests.put(license_url, cookies=self.crossref_cookies, data=json.dumps(license_input_updated), headers={"Content-Type": "application/json"})
            
            self.assertEqual(put_request.status_code, 201)

            license_response = requests.get(license_url, cookies=self.crossref_cookies)
            license_object = json.loads(license_response.text)

            for field in license_input.keys():
                self.assertTrue(license_object[field] == license_input_updated[field])


            # And GET it to check that it updated.
            license_response = requests.get(license_url, cookies=self.crossref_cookies)
            license_object = json.loads(license_response.text)

            for field in license_input.keys():
                self.assertTrue(license_object[field] == license_input_updated[field])

    def test_html_escape(self):
        """
        If angle brackets are present in the markdown they should be escaped for security reasons.
        """
        for _ in runs:
            license_input = {
                "title": "title",
                "full_text": "fulltext <script>alert('nope');</script>",
                "uri": "http://test_html_escape.com",
                "description": "description"
            }

            post_request = requests.post(ROUTE_ALL_LICENSES, cookies=self.crossref_cookies, data=json.dumps(license_input), headers={"Content-Type": "application/json"})

            self.assertEqual(post_request.status_code, 201)


            license_response = json.loads(post_request.text)

            # Response should contain escaped entities.
            self.assertEqual(license_response['full_text'],"fulltext &lt;script&gt;alert('nope');&lt;/script&gt;")

            # Re-get, should be the same.
            license_url = ROUTE_ALL_LICENSES + "/" + repr(license_response['id'])
            license_response = requests.get(license_url, cookies=self.crossref_cookies)
            license_object = json.loads(license_response.text)
            self.assertEqual(license_object['full_text'],"fulltext &lt;script&gt;alert('nope');&lt;/script&gt;")


    def test_draft_license_can_be_edited(self):
        """
        If a License is in the Draft status, it can be Edited.
        """
        for _ in runs:
            license_input = {
                "title": "first",
                "full_text": "first",
                "uri": "http://test_draft_license_can_be_edited.com",
                "description": "description"
            }

            # Create.
            post_request = requests.post(ROUTE_ALL_LICENSES, cookies=self.crossref_cookies, data=json.dumps(license_input), headers={"Content-Type": "application/json"})
            self.assertEqual(post_request.status_code, 201)
            license_response = json.loads(post_request.text)
            self.assertEqual(license_response['title'],"first")

            license_url = ROUTE_ALL_LICENSES + "/" + repr(license_response['id'])

            # Update.
            license_input["title"] = "second"
            license_response = requests.put(license_url, cookies=self.crossref_cookies, data=json.dumps(license_input), headers={"Content-Type": "application/json"})
            license_response = requests.get(license_url, cookies=self.crossref_cookies)

            license_object = json.loads(license_response.text)
            self.assertEqual(license_object['title'],"second")

            # Update again.
            license_input["title"] = "third"
            license_response = requests.put(license_url, cookies=self.crossref_cookies, data=json.dumps(license_input), headers={"Content-Type": "application/json"})
            license_response = requests.get(license_url, cookies=self.crossref_cookies)
            license_object = json.loads(license_response.text)
            self.assertEqual(license_object['title'],"third")


    def test_draft_license_can_be_deleted(self):
        """
        If a License is in the Draft status, it can be Deleted.
        """
        for _ in runs:
            license_input = {
                "title": "first",
                "full_text": "first",
                "uri": "http://test_draft_license_can_be_edited.com",
                "description": "description"
            }

            # Create.
            post_request = requests.post(ROUTE_ALL_LICENSES, cookies=self.crossref_cookies, data=json.dumps(license_input), headers={"Content-Type": "application/json"})
            self.assertEqual(post_request.status_code, 201)
            license_response = json.loads(post_request.text)

            license_url = ROUTE_ALL_LICENSES + "/" + repr(license_response['id'])

            # Update.
            license_input = {
                "title": "first",
                "full_text": "first",
                "uri": "http://test_draft_license_can_be_edited.com",
                "description": "description",
                "status": LICENSE_STATE_DELETED,
            }

            # Change to deleted state.
            delete_request = requests.put(license_url, cookies=self.crossref_cookies, data=json.dumps(license_input), headers={"Content-Type": "application/json"})

            # In the deleted state but still exists.
            self.assertEqual(post_request.status_code, 201)

            # Fetch, should be in deleted state.
            license_response = requests.get(license_url, cookies=self.crossref_cookies)
            self.assertEqual(post_request.status_code, 201)
            license_object = json.loads(license_response.text)
            self.assertEqual(license_object['status'], LICENSE_STATE_DELETED)

    def test_deleted_license_cannot_be_edited(self):
        """
        If a License is in the Deleted status, it can not be Edited.
        """
        for _ in runs:
            url, license = self.create_license_in_state(
                "http://example.com",
                LICENSE_STATE_DELETED,
                title="license title first",
            )

            # Update the title. 
            license["title"] = "second"

            # Error should result.
            returned = self.update_license(license, expected_status_code=409, should_return=False)

            # And fetch, again title should not have changed.
            refetched = self.refetch_license(license)
            self.assertEqual(refetched["title"], "license title first")


    def test_draft_license_can_be_published(self):
        """
        If a License is in the Draft status, it can be Published.
        """
        for _ in runs:
            # Create in deleted state.
            url, license = self.create_license_in_state(
                "http://example.com",
                None,
                title="license title first",
            )

            license['status'] = LICENSE_STATE_PUBLISHED
            update_response = self.update_license(license)

            # Should have updated.
            self.assertEqual(update_response["status"], LICENSE_STATE_PUBLISHED)

            # And fetch, again title should have changed.
            get_response = self.refetch_license(license)
            self.assertEqual(get_response["status"], LICENSE_STATE_PUBLISHED)        


    def test_published_license_cannot_be_edited(self):
        """
        If a License is in the Published status, it can not be Edited.
        """
        for _ in runs:
            # Create in published state.
            url, license = self.create_license_in_state(
                "http://example.com",
                LICENSE_STATE_PUBLISHED,
                title="first",
            )

            license["title"] = "Updated title"

            self.update_license(license, expected_status_code=409, should_return=False)

            
            # Title should not have changed.
            get_response = self.refetch_license(license)
            self.assertEqual(get_response["title"], "first")

    def test_published_license_can_be_drafted(self):
        """
        If a License is in the Published status, it can be Draft-ed to take it back to Draft, if it has not been accepted by any Researchers.
        """
        for _ in runs:
            # Create in published state.
            url, license = self.create_license_in_state(
                "http://example.com",
                LICENSE_STATE_PUBLISHED,
                title="first",
            )

            # Draft it.
            license['status'] = LICENSE_STATE_DRAFT
            self.update_license(license)
            self.assertEqual(self.refetch_license(license)['status'], LICENSE_STATE_DRAFT)

            # Change title.
            license['title'] = "second title"
            self.update_license(license)

            # Should have changed.
            self.assertEqual(self.refetch_license(license)['title'], "second title")

    def test_accepted_license_can_not_be_drafted(self):
        """
        If a License is in the Accepted status, it cannot be Draft-ed.
        """

        for _ in runs:
            url, license = self.create_license_in_state(
                "http://example.com",
                LICENSE_STATE_PUBLISHED,
                title="first",
            )

            self.review_license(license, STATUS_REVIEW_ACCEPTED)

            # Draft it.
            license['status'] = LICENSE_STATE_DRAFT

            # Shouldn't be allowed.
            self.update_license(license, expected_status_code=409, should_return=False)

    def test_non_draft_license_cannot_be_edited(self):
        """
        A license in any state other than draft cannot be edited.
        """
        for _ in runs:
            for state in [LICENSE_STATE_PUBLISHED, LICENSE_STATE_DELETED, LICENSE_STATE_RETIRED]:
                _, license = self.create_license_in_state(
                    "http://example-%d.com" % state,
                    state,
                    title="title-%d" % state,
                )

                license['title'] = "new title"
                self.update_license(license, expected_status_code=409, should_return=False)


    def test_accepted_license_cannot_be_edited(self):
        """
        If a License is in the Accepted status, it can not be Edited.
        """

        for _ in runs:
            url, license = self.create_license_in_state(
                "http://example.com",
                LICENSE_STATE_PUBLISHED,
                title="first",
            )

            self.review_license(license, STATUS_REVIEW_ACCEPTED)

            # Change the title.
            license['title'] = "new title"

            # Shouldn't be allowed.
            self.update_license(license, expected_status_code=409, should_return=False)


    def test_license_URIs_cannot_clash(self):
        """
        If a License exists in any status (draft, deleted, published, accepted, retired) another License cannot be created with the same URI.
        True regardless of creators.
        """

        for _ in runs:
            url, license = self.create_license_in_state(
                "http://example.com",
                None,
                title="first",
            )  

            second_license_info = {
                "title": "user one's license",
                "full_text": "first",
                "uri": "http://example.com",
                "description": "description",
                "status": LICENSE_STATE_DRAFT,
            }

            # Create the second one. Expect an error.
            post_request = requests.post(ROUTE_ALL_LICENSES, cookies=self.crossref_cookies, data=json.dumps(second_license_info), headers={"Content-Type": "application/json"})
            
            self.assertEqual(post_request.status_code, 400)

        
    def test_license_list_contains_all_own_licenses(self):
        """
        The 'list Licenses' endpoint lists all and only Licenses that belong to the Member and that are not Deleted.
        """
        for _ in runs:
            _, mine_1 = self.create_license_in_state(
                "http://example1.com",
                LICENSE_STATE_PUBLISHED,
                title="first",
            )

            _, mine_2 = self.create_license_in_state(
                "http://example2.com",
                LICENSE_STATE_PUBLISHED,
                title="second",
            )

            _, mine_3 = self.create_license_in_state(
                "http://example3.com",
                LICENSE_STATE_PUBLISHED,
                title="third",
            )

            _, not_mine_1 = self.create_license_in_state(
                "http://example4.com",
                LICENSE_STATE_PUBLISHED,
                title="fourth",
                cookies=self.crossref_cookies_2,
            )

            _, not_mine_2 = self.create_license_in_state(
                "http://example5.com",
                LICENSE_STATE_PUBLISHED,
                title="fifth",
                cookies=self.crossref_cookies_2,
            )

            _, not_mine_3 = self.create_license_in_state(
                "http://example6.com",
                LICENSE_STATE_PUBLISHED,
                title="sixth",
                cookies=self.crossref_cookies_2,
            )

            # My licenses and only my licenses.
            my_licenses = self.get_my_licenses()
            self.assertEqual(len(my_licenses), 3)
            for uri in ["http://example3.com", "http://example2.com", "http://example3.com"]:
                self.assertEqual(len(filter(lambda x:x['uri'] == uri, my_licenses)), 1)

            # Same for the other user.
            other_persons_licenses = self.get_my_licenses(cookies=self.crossref_cookies_2)
            self.assertEqual(len(other_persons_licenses), 3)
            for uri in ["http://example4.com", "http://example5.com", "http://example6.com"]:
                self.assertEqual(len(filter(lambda x:x['uri'] == uri, other_persons_licenses)), 1)

    def def_member_can_publish_only_own_licenses(self):
        """
        A Member cannot publish a license that does not belong to him.
        """

        license_input_a = {
            "title": "user one's license",
            "full_text": "first",
            "uri": "http://def_member_can_publish_only_own_licenses_A.com",
            "description": "description",
            "status": LICENSE_STATE_DRAFT,
        }

        # Create.
        post_request = requests.post(ROUTE_ALL_LICENSES, cookies=self.crossref_cookies, data=json.dumps(license_input_a), headers={"Content-Type": "application/json"})
        self.assertEqual(post_request.status_code, 201)
        license_response = json.loads(post_request.text)
        license_a_url = ROUTE_ALL_LICENSES + "/" + repr(license_response['id'])


        # Another with the same URI.
        license_input_b = {
            "title": "user one's license",
            "full_text": "with the first url",
            "uri": "http://def_member_can_publish_only_own_licenses_B.com",
            "description": "description",
            "status": LICENSE_STATE_DRAFT,
        }

        # Create with other user's creds.
        post_request = requests.post(ROUTE_ALL_LICENSES, cookies=self.crossref_cookies_2, data=json.dumps(license_input_b), headers={"Content-Type": "application/json"})
        self.assertEqual(post_request.status_code, 201)
        license_response = json.loads(post_request.text)
        license_b_url = ROUTE_ALL_LICENSES + "/" + repr(license_response['id'])

        # User 1 publishes his own, fine.
        license_input_a["status"] = LICENSE_STATE_PUBLISHED
        license_response = requests.put(license_a_url, cookies=self.crossref_cookies, data=json.dumps(license_input_a), headers={"Content-Type": "application/json"})
        self.assertEqual(license_response.status_code, 200)

        # User 1 publishes user 2's. Not fine.
        license_input_a["status"] = LICENSE_STATE_PUBLISHED
        license_response = requests.put(license_b_url, cookies=self.crossref_cookies, data=json.dumps(license_input_b), headers={"Content-Type": "application/json"})
        self.assertEqual(license_response.status_code, 401)

    def reviewed_license_cannot_be_redrafted(self, review_state):
        """
        Helper
        """
        
        _, license = self.create_license_in_state(
            "http://example.com",
            LICENSE_STATE_PUBLISHED,
        )

        # Cycle to draft and back to show we can.
        license["status"] = LICENSE_STATE_DRAFT
        self.update_license(license)

        license["status"] = LICENSE_STATE_PUBLISHED
        self.update_license(license)

        # Review it in one of the states.
        self.review_license(license, review_state)

        # Now attempt to draft. Should fail.
        license["status"] = LICENSE_STATE_DRAFT
        self.update_license(license, expected_status_code=409, should_return=False)

    def test_accepted_license_cannot_be_redrafted(self):
        """
        A member cannot re-draft a license that has been accepted by a Researcher.
        """
        for _ in runs:
            self.reviewed_license_cannot_be_redrafted(STATUS_REVIEW_ACCEPTED)

    def test_rejected_license_cannot_be_redrafted(self):
        """
        A member cannot re-draft a license that has been rejected by a Researcher.
        """
        for _ in runs:
            self.reviewed_license_cannot_be_redrafted(STATUS_REVIEW_ACCEPTED)

    def test_postponed_license_cannot_be_redrafted(self):
        """
        A member cannot re-draft a license that has been postponed by a Researcher.
        """
        for _ in runs:
            self.reviewed_license_cannot_be_redrafted(STATUS_REVIEW_ACCEPTED)


    def test_member_can_request_api_token(self):
        """
        A Member can request a Member API token. The same token is returned each request.
        """
        for _ in runs:
            token_original = requests.get(ROUTE_MEMBER_TOKEN, cookies=self.crossref_cookies).text
            
            self.assertTrue(len(token_original) > 0)

            # Get it a few times, should get the same token.
            for _ in range(10):
                token_again = requests.get(ROUTE_MEMBER_TOKEN, cookies=self.crossref_cookies).text

                self.assertEqual(token_original, token_again)

    def test_member_can_reissue_api_token(self):
        """
        A Member can request a Member API token is re-issued.
        A different token will be returned to the previous one. Subsequent calls to the Member Token API will return the new token.
        """
        for _ in runs:
            token_original = self.get_member_token()

            # Now revoke it.
            # Should get back an OK.
            self.assertEqual(requests.delete(ROUTE_MEMBER_TOKEN, cookies=self.crossref_cookies).status_code, 204)

            # Get it again. Should be different.
            reissued_token = self.get_member_token()

            self.assertNotEqual(token_original, reissued_token)

    def test_member_can_get_and_set_name(self):
        for _ in runs:

            publishers = [(self.crossref_cookies, "Intraglobal Publisher Corp"), (self.crossref_cookies_2, "Parochial House")]

            for cookies, name in publishers:
                # Initially empty name.
                name_request = requests.get(ROUTE_MEMBER_NAME, cookies=cookies)
                self.assertEqual(name_request.status_code, 200)
                self.assertEqual(name_request.text, "")

                # Then set it.
                name_request = requests.put(ROUTE_MEMBER_NAME, cookies=cookies, data=name)
                self.assertEqual(name_request.status_code, 201)

                # Then get it.
                name_request = requests.get(ROUTE_MEMBER_NAME, cookies=cookies)
                self.assertEqual(name_request.status_code, 200)
                self.assertEqual(name_request.text, name)

            # Make sure they're not interfering.
            for cookies, name in publishers:
                # Then get it.
                name_request = requests.get(ROUTE_MEMBER_NAME, cookies=cookies)
                self.assertEqual(name_request.status_code, 200)
                self.assertEqual(name_request.text, name)



class TestLicensesResearcher(CrossRefBits):
    """
    Tests for handling of licenses from the Researcher's point of view.
    """

    def test_can_only_list_published_licenses(self):
        """
        The 'list Licenses' endpoints (not accepted, postponed, accepted) lists Licenses iff they are Published.
        """

        for _ in runs:
            # Create some licenses.
            for license_state in (LICENSE_STATE_DRAFT, LICENSE_STATE_PUBLISHED, LICENSE_STATE_DELETED, LICENSE_STATE_RETIRED):
                for i in range(10):

                    license_input = {
                        "title": "user one's license",
                        "full_text": "first",
                        "uri": "http://test_can_only_list_published_licenses-%d-%d.com" % (license_state, i),
                        "description": "description",
                        "status": LICENSE_STATE_DRAFT,
                    }

                    post_request = requests.post(ROUTE_ALL_LICENSES, cookies=self.crossref_cookies, data=json.dumps(license_input), headers={"Content-Type": "application/json"})
                    self.assertEqual(post_request.status_code, 201)
                    
                    license_response = json.loads(post_request.text)
                    url = ROUTE_ALL_LICENSES + "/" + repr(license_response['id'])

                    # Change to new state.
                    if license_state == LICENSE_STATE_DRAFT:
                        # Already a draft.
                        pass
                    elif license_state == LICENSE_STATE_RETIRED:
                        # Need to go via Published first.

                        license_input['status'] = LICENSE_STATE_PUBLISHED
                        license_response = requests.put(url, cookies=self.crossref_cookies, data=json.dumps(license_input), headers={"Content-Type": "application/json"})
                        self.assertEqual(license_response.status_code, 201)

                        license_input['status'] = LICENSE_STATE_RETIRED
                        license_response = requests.put(url, cookies=self.crossref_cookies, data=json.dumps(license_input), headers={"Content-Type": "application/json"})

                        self.assertEqual(license_response.status_code, 201)
                    else:
                        # Switch directly to that state.
                        license_input['status'] = license_state
                        license_response = requests.put(url, cookies=self.crossref_cookies, data=json.dumps(license_input), headers={"Content-Type": "application/json"})

                        self.assertEqual(license_response.status_code, 201)


            for_review = requests.get(ROUTE_REVIEWS_UNREVIEWED, cookies=self.orcid_cookies)
            
            self.assertTrue(for_review.status_code, 200)

            reviews = json.loads(for_review.text)

            # Only 10 of these are published, only those 10 should have been returned.
            self.assertEqual(len(reviews), 10)
            for review in reviews:
                self.assertEqual(review['license_status'], LICENSE_STATE_PUBLISHED)

    def test_researcher_can_accept_published_license(self):
        """
        A Researcher can accept a License if it is Published.
        """
        for _ in runs:
            URI = "http://test.com"

            license_url, license = self.create_license_in_state(URI, LICENSE_STATE_PUBLISHED)

            self.assertEqual(len(filter(lambda x:x['uri'] == URI, self.reviews_in_state(STATUS_REVIEW_NOT_REVIEWED))), 1)
            self.assertEqual(len(filter(lambda x:x['uri'] == URI, self.reviews_in_state(STATUS_REVIEW_ACCEPTED))), 0)

            self.review_license(license, STATUS_REVIEW_ACCEPTED)

            # Now get the review, should be in the correct state.
            accepted = self.reviews_in_state(STATUS_REVIEW_ACCEPTED)
            self.assertEqual(len(filter(lambda x:x['uri'] == URI, self.reviews_in_state(STATUS_REVIEW_NOT_REVIEWED))), 0)
            self.assertEqual(len(filter(lambda x:x['uri'] == URI, self.reviews_in_state(STATUS_REVIEW_ACCEPTED))), 1)


    def test_researcher_can_not_accept_retired_license(self):
        """
        A Researcher can not Accept a License if it is Retired.
        """
        for _ in runs:
            license_url, license = self.create_license_in_state("http://test.com", LICENSE_STATE_RETIRED)

            # 409 because the license is allowed to be GOT but not reviewed.
            self.review_license(license, STATUS_REVIEW_ACCEPTED, expected_status_code=409)


    def test_review_nonexistent_license_409(self):
        """
        Reviewing a nonexistent license return a 409.
        """
        for _ in runs:
            license_url, license = self.create_license_in_state("http://test.com", LICENSE_STATE_PUBLISHED)

            # According to the Liberator docs, 404 isn't valid for PUT.
            license['uri'] = "http://a-different-url.com"
            self.review_license(license, STATUS_REVIEW_ACCEPTED, expected_status_code=409)


    def test_researcher_can_postpone_license(self):
        """
        A Researcher can postpone a license IFF it is Published.
        """
        for _ in runs:
            license_url, license = self.create_license_in_state("http://test.com", LICENSE_STATE_PUBLISHED)

            self.review_license(license, STATUS_REVIEW_POSTPONED)

    def test_researcher_can_reject_license(self):
        """
        A Researcher can reject a license IFF it is Published and it has been Postponed or not Accepted.
        """
        for _ in runs:
            license_url, license = self.create_license_in_state("http://test.com", LICENSE_STATE_PUBLISHED)

            self.review_license(license, STATUS_REVIEW_REJECTED)


    def test_researcher_can_accept_license_after_rejecting(self):
        """
        A Researcher can accept a license after rejecting it.
        """
        for _ in runs:
            license_url, license = self.create_license_in_state("http://test.com", LICENSE_STATE_PUBLISHED)

            self.review_license(license, STATUS_REVIEW_REJECTED)

            self.review_license(license, STATUS_REVIEW_ACCEPTED)

    def test_researcher_can_request_api_token(self):
        """
        A Researcher can request a Researcher API token. The same token is returned each request.
        """

        for _ in runs:
            token_original = requests.get(ROUTE_RESEARCHER_TOKEN, cookies=self.orcid_cookies).text
            
            self.assertTrue(len(token_original) > 0)

            # Get it a few times, should get the same token.
            for _ in range(10):
                token_again = requests.get(ROUTE_RESEARCHER_TOKEN, cookies=self.orcid_cookies).text

                self.assertEqual(token_original, token_again)

    def test_researcher_can_reissue_api_token(self):
        """
        A Member can request a Researcher API token is re-issued.
        A different token will be returned to the previous one. Subsequent calls to the Researcher Token API will return the new token.
        """
        for _ in runs:
            token_original = self.get_researcher_token()

            # Now revoke it.
            # We should get back a 'no content' response.
            self.assertEqual(requests.delete(ROUTE_RESEARCHER_TOKEN, cookies=self.orcid_cookies).status_code, 204)

            # Get it again. Should be different.
            reissued_token = self.get_researcher_token()

            self.assertNotEqual(token_original, reissued_token)



class TestPublisherAPI(CrossRefBits):
    """
    Tests for the Publisher API.
    """

    def test_request_license_for_user_requires_both_tokens_correct(self):
        """
        Requesting the License For User returns 401 if the Member Token is not valid.
        """
        for _ in runs:

            for token_header in PUBLISHER_TOKEN_HEADER_FIELDS:

                # Correct tokens should work OK.
                researcher_token = self.get_researcher_token()
                publisher_token = self.get_member_token()

                req = requests.get(ROUTE_API_LICENSES + researcher_token, headers = {token_header: publisher_token})
                self.assertEqual(req.status_code, 200)

                # Bad researcher token should not work.
                researcher_token = "NOT-THE-CORRECT-TOKEN"
                publisher_token = self.get_member_token()

                req = requests.get(ROUTE_API_LICENSES + researcher_token, headers = {token_header: publisher_token})
                self.assertEqual(req.status_code, 404)

                # Bad member token should not work.
                researcher_token = self.get_researcher_token()
                publisher_token = "ABSOLUTELY-NOT-THE-CORRECT-TOKEN"

                req = requests.get(ROUTE_API_LICENSES + researcher_token, headers = {token_header: publisher_token})
                self.assertEqual(req.status_code, 404)

    def test_request_licenses_for_researcher_returns_publishers_and_global(self):
        """
        Requesting the Licenses for Researcher returns all licenses that the User has reviewed that belong to the Publisher and are Global.
        Should include the user's ORCID.
        For each license should include:
         - the URI of the license
         - the status of the license (rejected, accepted)
         - datetime that it was reviewed at in ISO8601
        """

        for _ in runs:
            for token_header in PUBLISHER_TOKEN_HEADER_FIELDS:
                # Set-up will create two users.
                # Make one of them a staff user. This is messy, but simplest.
                self.make_staff_user("testuser_2")

                researcher_token = self.get_researcher_token()

                # The publisher's token will be for the non-staff member.
                publisher_token = self.get_member_token()


                _, license_1 = self.create_license_in_state(
                    "http://example1.com" + token_header,
                    LICENSE_STATE_PUBLISHED,
                )

                _, license_2 = self.create_license_in_state(
                    "http://example2.com" + token_header,
                    LICENSE_STATE_PUBLISHED,
                )

                _, staff_license_1 = self.create_license_in_state(
                    "http://example_staff_1.com" + token_header,
                    LICENSE_STATE_PUBLISHED,
                    cookies=self.crossref_cookies_2,
                )

                _, staff_license_2 = self.create_license_in_state(
                    "http://example_staff_2.com" + token_header,
                    LICENSE_STATE_PUBLISHED,
                    cookies=self.crossref_cookies_2,
                )

                self.review_license(license_1, STATUS_REVIEW_ACCEPTED)
                self.review_license(license_2, STATUS_REVIEW_ACCEPTED)
                self.review_license(staff_license_1, STATUS_REVIEW_ACCEPTED)
                self.review_license(staff_license_2, STATUS_REVIEW_ACCEPTED)

                # Now get the API results.
                response = self.api_fetch(token_header)

                self.assertEqual(response['result'], "ok")
                self.assertEqual(response['message'], "licenses")

                # Orcid configured in mockauth's configuration.
                # self.assertEqual(response['orcid'], "0000-1234-0000-4321")

                # We are passing in the token for the non-staff user but we
                # should get licenses that belong to us and to the staff member.

                licenses = response['licenses']

                for uri, review_status in [
                    ("http://example1.com" + token_header, "accepted"),
                    ("http://example2.com" + token_header, "accepted"),
                    ("http://example_staff_1.com" + token_header, "accepted"),
                    ("http://example_staff_2.com" + token_header, "accepted")]:

                    wanted = len(filter(
                        lambda x: x['uri'] == uri and x['status'] == review_status,
                        licenses
                    ))

                    self.assertEqual(wanted, 1)

    def test_request_licenses_for_researcher_with_obsolete_researcher_token(self):
        """
        Requesting the License for Publisher with a previously valid but since superceded Researcher token returns a 400.
        """

        for _ in runs:
            for token_header in PUBLISHER_TOKEN_HEADER_FIELDS:
                # Correct tokens should work OK.
                researcher_token = self.get_researcher_token()
                publisher_token = self.get_member_token()

                req = requests.get(ROUTE_API_LICENSES + researcher_token, headers = {token_header: publisher_token})

                self.assertEqual(req.status_code, 200)

                # Now revoke the re-issue the token.
                requests.delete(ROUTE_RESEARCHER_TOKEN, cookies=self.orcid_cookies)

                # Bad request.
                req = requests.get(ROUTE_API_LICENSES + researcher_token, headers = {token_header: publisher_token})
                self.assertEqual(req.status_code, 404)

                # Get the new one.
                researcher_token = self.get_researcher_token()
                
                # Good request.
                req = requests.get(ROUTE_API_LICENSES + researcher_token, headers = {token_header: publisher_token})
                
                self.assertEqual(req.status_code, 200)

    def test_request_license_with_obsolete_member_token(self):
        """
        Requesting the License for Publisher with a previously valid but since superceded Member token returns a 401.
        """

        for _ in runs:
            for token_header in PUBLISHER_TOKEN_HEADER_FIELDS:
                # Correct tokens should work OK.
                researcher_token = self.get_researcher_token()
                publisher_token = self.get_member_token()

                req = requests.get(ROUTE_API_LICENSES + researcher_token, headers = {token_header: publisher_token})
                self.assertEqual(req.status_code, 200)

                # Now revoke the re-issue the token.
                requests.delete(ROUTE_MEMBER_TOKEN, cookies=self.crossref_cookies)

                # Bad request.
                req = requests.get(ROUTE_API_LICENSES + researcher_token, headers = {token_header: publisher_token})
                self.assertEqual(req.status_code, 404)

                # Get the new one.
                publisher_token = self.get_member_token()
                
                # Good request.
                req = requests.get(ROUTE_API_LICENSES + researcher_token, headers = {token_header: publisher_token})
                self.assertEqual(req.status_code, 200)

    def test_request_license_includes_retired(self):
        """
        Requesting the Licenses for Researcher will return all licenses that have been Retired since they were accepted
        """

        for _ in runs:
            for token_header in PUBLISHER_TOKEN_HEADER_FIELDS:
                _, license_1 = self.create_license_in_state(
                    "http://example1.com" + token_header,
                    LICENSE_STATE_PUBLISHED,
                )

                _, license_2 = self.create_license_in_state(
                    "http://example2.com" + token_header,
                    LICENSE_STATE_PUBLISHED,
                )

                _, license_3 = self.create_license_in_state(
                    "http://example3.com" + token_header,
                    LICENSE_STATE_PUBLISHED,
                )

                _, license_4 = self.create_license_in_state(
                    "http://example4.com" + token_header,
                    LICENSE_STATE_PUBLISHED,
                )

                self.review_license(license_1, STATUS_REVIEW_ACCEPTED)
                self.review_license(license_2, STATUS_REVIEW_ACCEPTED)
                self.review_license(license_3, STATUS_REVIEW_POSTPONED)
                self.review_license(license_4, STATUS_REVIEW_REJECTED)

                # Don't retire number 2.
                license_1['status'] = LICENSE_STATE_RETIRED
                license_3['status'] = LICENSE_STATE_RETIRED
                license_4['status'] = LICENSE_STATE_RETIRED
                self.update_license(license_1)
                self.update_license(license_3)
                self.update_license(license_4)

                # Now get the API results.
                response = self.api_fetch(token_header)

                self.assertEqual(response['result'], "ok")
                self.assertEqual(response['message'], "licenses")

                # Orcid configured in mockauth's configuration.
                # ORCID currrently not being returned from new API. May be replaced.
                # self.assertEqual(response['orcid'], "0000-1234-0000-4321")

                licenses = response['licenses']

                for uri, review_status in [
                    ("http://example1.com" + token_header, "accepted"),
                    ("http://example2.com" + token_header, "accepted"),
                    ("http://example3.com" + token_header, "postponed"),
                    ("http://example4.com" + token_header, "rejected")]:

                    wanted = len(filter(
                        lambda x: x['uri'] == uri and x['status'] == review_status,
                        licenses
                    ))

                    self.assertEqual(wanted, 1)


if __name__ == '__main__':
    unittest.main()
