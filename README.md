# CrossRef Agreements

The CrossRef Agreements service for use with TDM clickthrough. Serves the API for the UI for Researchers and Publishers, also the API for license-checking by publishers. This also includes the Publisher and Researcher Angular projects.

## Usage

Configuration is done via an EDN file, `config.edn`, in the same directory. When deploying, make a copy of the `config.edn.example` file, which has production defaults. When this does not exist, the service will fall back to the development settings, `config.dev.edn`. 

    $ cd /path/to/project/root
    $ lein run

or

    $ cd /path/to/project/root
    $ lein ring server-headless 3000

or, in production:

    $ cd /path/to/project/root
    $ lein daemon start clickthrough

and

    $ lein daemon stop clickthrough

## Configuration

Sensible production values are hard-coded for nearly all values. You can override all values in the config file if you want.

The following keys must be set:

    :orcid-client-id
    :orcid-client-secret
    :agreements-db-username
    :agreements-db-password

The following keys can be set (NB the leading colons):

    :crossref-login-url
    :orcid-scope
    :orcid-authorize-url
    :orcid-token-url
    :orcid-redirect-url-route
    :orcid-redirect-url
    :allowed-origin
    :agreements-db-name
    :agreements-db-host
    :agreements-db-port
    :api-base
    :reviewers-base-url
    :route-prefix


### crossref-login-url

Full URL of CrossRef login service. e.g.

    "https://auth.labs.crossref.org/login"

### orcid-scope

ORCID OAuth2 scope. e.g. 

    "/orcid-profile/read-limited"

### orcid-authorize-url

ORCID OAuth2 authorise url. e.g. 

    "https://orcid.org/oauth/authorize"

### orcid-token-url

ORCID OAuth2 token url. e.g. 

    "https://api.orcid.org/oauth/token"

### orcid-redirect-url-route

Route the app listens on for ORCID OAuth2 redirect. This should not include the `ROOT-PREFIX` if there is one (in this case, `/agreements`). e.g. 

    "/api/auth/orcid/callback"

### orcid-redirect-url

Full URL corresponding to `ORCID-REDIRECT-URL-ROUTE`. Given to ORCID OAuth2. e.g. 

    "https://apps.crossref.org/agreements/api/auth/orcid/callback"

### allowed-origin

Allowed HTTP origin. Should be domain of the server where this is hosted. e.g. 

    "https://apps.crossref.org"

### agreements-db-name, agreements-db-host, agreements-db-port

MySQL database name, hostname, port

### agreements-db-username

MySQL database username

### agreements-db-password

MySQL database password

### api-base

Full URL of the API base where this is served. This is passed into the JavaScript app so it knows where to find the Clojure server. Must include the `ROOT-PREFIX` if there is one.

    "https://apps.crossref.org/agreements/api/"

### reviewers-base-url

Full URL of the URL where researchers are sent to review a license. A fragment in the Angular.js app. e.g.

    "https://apps.crossref.org/agreements/researchers/index.html#/tc/"

### route-prefix

Prefix for all routes relative to the URL root. Used if this is reverse-proxied to be hosted not at the URL root. E.g., as for above examples:

    "/agreements"


### orcid-client-id

ORCID OAuth2 client id

### orcid-client-secret

ORCID OAuth2 client secret

## API Tests

These are Python. You'll need the CrossRef [Mock Authentication service](https://github.com/CrossRef/mockauth) to be running (which is written in Go). You don't need to worry about this in production.

1. Make sure `mockauth` is running.
2. Run the server with with ./run-dev.sh
3. Run `./api-test.sh`. This will run the Python API tests and will clobber any data you have the database. 
