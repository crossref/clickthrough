'use strict';

/* Controllers */

var controllers = angular.module('agreementsForPublishers.controllers', ['restangular']);

// Lists of license items.
controllers.controller('LicenseListController', function($scope, $rootScope, $filter, $location, $http, LicensesAPI) {
	$rootScope.currentPage = "license-list";

	// Null publisher name distinct from empty string.
	$scope.publisherName = null;
	$scope.publisherNameField = null;

	$scope.orderGlobalByField = "title";
	$scope.orderNonGlobalByField = "title";
	$scope.reverseNonGlobalSort = false;
	$scope.reverseGlobalSort = false;

	var publisherNameAPI = LicensesAPI.one('member/my/publisher/name');
	publisherNameAPI.get()
	.then(function(name) {
		// Empty string response intepreted as undefined.
		$scope.publisherName = name || "";
		$scope.publisherNameField = name || "";
	}, function() {
		// Can't fetch the name.
		$location.path("/error/internal")
	})

	$scope.editOrgName = false;
	$scope.startEditOrgName = function() {
		$scope.editOrgName = true;
	};

	$scope.saveNameField = function() {
		$http({method: 'PUT', headers: {"Content-Type": "text/plain"}, data: $scope.publisherNameField, withCredentials: true, url: window.CROSS_REF.API_BASE + "member/my/publisher/name"}).
				success(function(data, status, headers, config) {
					$scope.publisherName = $scope.publisherNameField;
					$scope.editOrgName = false;
	  			}).
				error(function(data, status, headers, config) {
					$location.path("/error/internal");
					$scope.editOrgName = false;
				});

	};

	$scope.cancelNameField = function() {
		$scope.publisherNameField = $scope.publisherName;
		$scope.editOrgName = false;
	};


	var licenses = LicensesAPI.all("member/licenses").all("mine");

	var globalLicenses = LicensesAPI.all("member/licenses").all("global");

	// On load do an initial fetch.
	licenses.getList()
	.then(function(theLicenses) {
		$scope.licenses = _.filter(theLicenses, function(x) {return x.status != $scope.consts.LICENSE_STATUS_DELETED});
	})

	globalLicenses.getList()
	.then(function(theLicenses) {
		$scope.community = theLicenses;
	})
}); 

// View, license. 
controllers.controller('LicenseViewController', function($scope, $rootScope, $routeParams, $location, LicensesAPI) {
	$rootScope.currentPage = "license-view";

	var LicensesAPI = LicensesAPI;

	var license = undefined;
	if ($routeParams.id !== undefined) {
		LicensesAPI.one('member/licenses', $routeParams.id).get()
		.then(function(license) {
		 	$scope.license = license;
		}, function() {
			// Can't fetch the license.
			$location.path("/error/internal")
		})

		// Fetch the Global license info (i.e. whether it's being used or not).
		// If there's an error then it's not a global license, no matter.
		LicensesAPI.one('member/licenses/global', $routeParams.id).get()
		.then(function(globalLicense) {
			$scope.globalLicense = globalLicense;
		})
	}

	$scope.edit = function() {
		if ($scope.license !== undefined) {
			$location.path("/tc/" + $scope.license.id + "/edit")
		}
	};
	
	$scope.publish = function() {
		if ($scope.license !== undefined) {
			if ($scope.license.status == $scope.consts.LICENSE_STATUS_DRAFT) {

				$scope.license.status = $scope.consts.LICENSE_STATUS_PUBLISHED;

				$scope.license.put().then(function() {
					$location.path("/tc");
				  }, function() {
				    $location.path("/error/internal")
				  });
			} else {			
				// We've been sent a license in a bad format.
				$location.path("/error/internal")
			}
		}
	};

	$scope.delete = function() {
		if ($scope.license !== undefined) {
			if ($scope.license.status == $scope.consts.LICENSE_STATUS_DRAFT) {

				$scope.license.status = $scope.consts.LICENSE_STATUS_DELETED;

				$scope.license.put().then(function() {
					$location.path("/tc");
				  }, function() {
				    console.log("There was an error saving");
				  });
			} else {			
				// We've been sent a license in a bad format.
				$location.path("/error/internal")
			}
		}
	};

	$scope.undelete = function() {
		if ($scope.license !== undefined) {
			if ($scope.license.status == $scope.consts.LICENSE_STATUS_DELETED) {

				$scope.license.status = $scope.consts.LICENSE_STATUS_DRAFT;

				$scope.license.put().then(function() {
					$location.path("/tc");
				  }, function() {
				    console.log("There was an error saving");
				  });
			} else {			
				// We've been sent a license in a bad format.
				$location.path("/error/internal")
			}
		}
	};

	$scope.redraftError = null;
	$scope.redraft = function() {
		if ($scope.license !== undefined) {
			if ($scope.license.status == $scope.consts.LICENSE_STATUS_PUBLISHED) {

				$scope.license.status = $scope.consts.LICENSE_STATUS_DRAFT;

				$scope.license.put().then(function() {
					$location.path("/tc/" + $scope.license.id + "/edit");
				  }, function() {
				  	// Revert to previous state.
				  	$scope.license.status = $scope.consts.LICENSE_STATUS_PUBLISHED;
				    $scope.redraftError = "You cannot redraft this license because it has already been reviewed by one or more Researchers.";
				  });
			} else {
				// Pass
			};
		};
	};

	$scope.retireError = null;
	$scope.retire = function() {
		if ($scope.license !== undefined) {
			if ($scope.license.status == $scope.consts.LICENSE_STATUS_PUBLISHED) {

				$scope.license.status = $scope.consts.LICENSE_STATUS_RETIRED;

				$scope.license.put().then(function() {
					$location.path("/tc/" + $scope.license.id);
				  }, function() {
				  	$scope.license.status = $scope.consts.LICENSE_STATUS_PUBLISHED;
				    $scope.retireError = "There was a problem retiring your terms and conditions.";
				  });
			} else {
				// Pass
			};
		};
	};

	// Use global license.
	$scope.use = function(isUsed) {
		var license = $scope.globalLicense;

		if (license) {
			license.used = isUsed;
			license.put().then(function(){$location.path("/tc")});
		}
	};
});


controllers.controller('LicenseEditController', function($scope, $routeParams, $location, $rootScope, LicensesAPI) {
	$rootScope.currentPage = "license-edit";

	var LicensesAPI = LicensesAPI;

	var licenses = LicensesAPI.all("member/licenses");

	// This can be 'markdown' or 'wysiwyg'.
	$scope.editType = 'wysiwyg';

	// Get an existing license or create a new one.
	if ($routeParams.id === undefined) {
		$scope.license = {
			title: "",
			full_text: "",
			description: "",
			uri: ""
		}
	} else {
		LicensesAPI.one('member/licenses', $routeParams.id).get()
		.then(function(license) {
		  $scope.license = license;
		}, function() {
			// Can't fetch the license.
			$location.path("/error/internal")
		})
	}
	
	$scope.$on('droppedText', function(event, text) {
		$scope.$apply(function() {
			$scope.license.full_text = text;
		});
	});

	// When the save button is clicked, save and return to list screen.
	$scope.errorText = null;
	$scope.save = function() {
		if ($scope.license.put) {
			$scope.license.put().then(function() {
					$location.path("/tc");
			  }, function(response) {
			  	if (response.data.status == "error") {
					$scope.errorText = response.data.error;
			  	} else {
			  		// This shouldn't happen if the validation has worked.
			  		$scope.errorText = "There was a problem with the data you supplied."
			  	}
			  });
		} else {
			licenses.post($scope.license).then(function() {
				$location.path("/tc");
			}, function(response) {

				if (response.data.status == "error") {
					$scope.errorText = response.data.error;
			  	} else {
			  		// This shouldn't happen if the validation has worked.
			  		$scope.errorText = "There was a communicating with the server. "
			  	}
			})
		}
}}); 

// Log in.

// Log in for CrossRef authentication.
controllers.controller('CrossRefLoginController', function($scope, $rootScope, $http, $filter, $location, AuthenticationService) {
	$rootScope.currentPage = "login";

	$scope.username = "";
	$scope.password = "";

	$scope.errorMessage = null;
	
	$scope.login = function() {
		$scope.errorMessage = null;

		var req = $filter("json")({"username": $scope.username, "password": $scope.password});

		$http({method: 'POST', data: req, withCredentials: true, url: window.CROSS_REF.LOG_IN_CROSS_REF}).
			success(function(data, status, headers, config) {
				AuthenticationService.crossRef.setLoggedIn(true)
				$location.path('/tc');				
  			}).
			error(function(data, status, headers, config) {
			  	$scope.errorMessage = "Sorry, your login details were refused."
			  	AuthenticationService.crossRef.setLoggedIn(false);
			});
	}
});

controllers.controller('ApiTokenController', function($scope, $rootScope, $http, LicensesAPI) {
	$rootScope.currentPage = "api-token";

		LicensesAPI.one('member/my/member/token').get()
		.then(function(token) {
		  $scope.token = token;
		}, function() {
			// Can't fetch the token.
			$location.path("/error/internal")
		})

		// Revoke and re-fetch.
		$scope.revokeToken = function() {
			$http({method: 'DELETE', withCredentials: true, url: window.CROSS_REF.API_BASE + 'member/my/member/token'}).
				success(function(data, status, headers, config) {

					LicensesAPI.one('member/my/member/token').get()
					.then(function(token) {
						$scope.token = token;
					}, function() {
					// Can't get the new token.
						$location.path("/error/internal")
					})
	  			}).
				error(function(data, status, headers, config) {
					// Can't revoke the token.
					$location.path("/error/internal")
				});
			};
});
