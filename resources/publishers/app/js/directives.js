'use strict';

/* Directives */

var directives = angular.module('agreementsForPublishers.directives', []);

// Log out button. Calls the logout method when clicked.
directives.directive('logoutButton', function(AuthenticationService) {
  return {
	controller: function($scope, $element){
		$scope.logout = function(){

			// This will call the logout endpoint.
			// The response will trigger the redirect in the app.
     	AuthenticationService.crossRef.logout();
		}
	},
	templateUrl: "partials/logout-button.html",
  link: function (scope, elem, attrs) {
	}
  }
});

directives.directive('licenseStatusBadge', function() {
    return {
        restrict: 'E',
        controller: function($scope) {
        },
        link: function(scope, element, attrs, $scope) {
          scope.statusText = "undefined";

          attrs.$observe('status', function(status) {
              if(status) {
                switch (parseInt(status, 10)) {
                    case scope.consts.LICENSE_STATUS_DRAFT: 
                        scope.statusText = "Draft"; break;
                    case scope.consts.LICENSE_STATUS_PUBLISHED:
                        scope.statusText = "Published"; break;
                    case scope.consts.LICENSE_STATUS_RETIRED:
                        scope.statusText = "Retired"; break;
                    case scope.consts.LICENSE_STATUS_DELETED:
                        scope.statusText = "Deleted"; break;
                }
            }
          });
        },
        template: '<span class="badge badge-inverse">{{statusText}}</span>'
    };
});

 
directives.directive('halloEditor', function() {
    return {
        restrict: 'A',
        require: '?ngModel',
        link: function(scope, element, attrs, ngModel) {
            if (!ngModel) {
                return;
            }

            var markdownize = function(content) {          

              var html = content.split("\n").map($.trim).filter(
                function(line) { 
                  return line != "";
                }).join("\n");
                return toMarkdown(html);
            };

            element.hallo({
               plugins: {
                 'halloformat': {"bold": true, "italic": true, "strikethrough": true, "underline": true},
                 'halloheadings': [1,2,3],
                 'hallojustify' : {}
               }
            });
 
            ngModel.$render = function() {
              // debugger
              try {
                element.html(marked(ngModel.$viewValue || ''));
              } catch (e) {
                // Syntax error may be raised mid-way through typing a document or for badly formatted markup.
              }
            };
 
            element.on('hallodeactivated', function() {
              ngModel.$setViewValue(html2markdown(element.html()))
                scope.$apply();
            });
        }
    };
});

directives.directive('markdownView', function() {
    return {
        restrict: 'A',
        require: '?ngModel',
        link: function(scope, element, attrs, ngModel) {
            if (!ngModel) {
                return;
            }
 
            ngModel.$render = function() {
              var converter = new Showdown.converter();
              try {
                element.html(marked(ngModel.$viewValue || ''));
              } catch (e) {
                // Error can be raised from bad markdown.
              }
            };

        }
    };
});

// Make the element accept text file drops, raise 'droppedText' event on the $rootScope.
directives.directive('dropTarget', function() {
    return {
        require: '?ngModel',
        link: function($scope, element, attrs, ngModel) {
          element[0].addEventListener('dragover',function(e){
            if(e.preventDefault) {
              e.preventDefault();
            }
            
            if(e.stopPropagation){
              e.stopPropagation();
            }

            e.dataTransfer.dropEffect='copy';
          });

          element[0].addEventListener('drop', function(e) {

            if(e.preventDefault) {
              e.preventDefault();
            }

            if(e.stopPropagation) {
              e.stopPropagation();
            }

            var fileList = e.dataTransfer.files;
            
            if (fileList.hasOwnProperty(0)) {
              var reader = new FileReader();
              var theFile = fileList[0];
              var slice = theFile.slice(0, theFile.size-1);

              reader.onload = function() {
                $scope.$broadcast('droppedText', reader.result);
              }

              reader.readAsText(slice);
            }
        });
      }
    }
});


directives.directive('statusBadge', function() {
    return {
        restrict: 'E',
        controller: function($scope) {
        },
        link: function(scope, element, attrs, $scope) {
          scope.statusText = "undefined";
          
          scope.$watch('license', function(license) {
            if(license) {
              switch (license.status) {
                  case scope.consts.LICENSE_STATUS_DRAFT: 
                      scope.statusText = "Draft"; break;
                  case scope.consts.LICENSE_STATUS_PUBLISHED:
                      scope.statusText = "Published"; break;
                  case scope.consts.LICENSE_STATUS_RETIRED:
                      scope.statusText = "Retired"; break;
                  case scope.consts.LICENSE_STATUS_DELETED:
                      scope.statusText = "Deleted"; break;
              }
            }
          })
        },
        template: '{{statusText}}'
    };
});

directives.directive('sortMarker', ["$sce", function($sce) {
    return {
        restrict: 'E',
        scope: true,

        controller: function($scope) {
        },
        link: function(scope, element, attrs, $scope) {
          scope.order = undefined;
          scope.by = "";
          scope.down = false;
          scope.up = false;

          scope.column = attrs.column;

          scope.update = function() {
            if (attrs.column == scope.by) {
              if (scope.order === true) {            
                scope.up = true;
                scope.down = false;
              } else if (scope.order === false) {
                scope.up = false;
                scope.down = true;
              } 
            } else {
                scope.up = false;
                scope.down = false;
            }
          };

          scope.$watch(attrs.order, function(order) {
            scope.order = order;
            scope.update();
          })
        
          scope.$watch(attrs.by, function(by) {
            scope.by = by;
            $scope.test = scope.by;
            scope.update();
          }); 
        },
        template: '<span ng-show="down">&#9650;</span><span ng-show="up">&#9660;</span>'
    };
}]);