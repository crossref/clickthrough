'use strict';

/* Services */

var services = angular.module('agreementsForPublishers.services', []);

// Authentication service. Keep track of the login state.
app.factory('AuthenticationService', function($http, $location, $rootScope) {
  
  // Create a crossRefLoginState in the rootScope.
  // This is watched in the login button directive.
  $rootScope.crossRefLoginState |= undefined;

  // State for CrossRef.
  // (this may co-exist with ORCID auth too).
  var crossRefState = {
    // Return true, false or undefined if not yet sure.
    isLoggedIn: function() {
      return $rootScope.crossRefLoginState;
    },

    // Set login state.
    setLoggedIn: function(isLoggedIn) {
      $rootScope.crossRefLoginState = isLoggedIn;
      if (isLoggedIn) {
        $http({method: 'GET', withCredentials: true, url: window.CROSS_REF.API_BASE + "member/me"}).success(function(data) {
          if (data.status && data.status == "ok") {
             $rootScope.loggedInUsername = data.user.username;
          }
        });
      }
    },

    // Log out with service. The response will invoke a route change.
    // The response should be a 401, which will be handled and $rootScope.crossRefLoginState will be set false.
    logout: function() {
      $http({
        method: 'GET',
        withCredentials: true,
        url: window.CROSS_REF.LOG_OUT
      });
      $rootScope.loggedInUsername = null;
    }
    };

  // When setting up, attempt to talk to the authentication service.
  // The response handler will act based on this.
  $http({
    method: 'GET',
    withCredentials: true,
    url: window.CROSS_REF.LOG_IN_CROSS_REF
  }).success(function(data, status, headers, config) {
    crossRefState.setLoggedIn(true);
  }).error(function(data, status, headers, config) {
    crossRefState.setLoggedIn(false);
  });


  return {
  	crossRef: crossRefState  
  };
});