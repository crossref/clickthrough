'use strict';

var app = angular.module(
  'agreementsForPublishers',
  [
    'agreementsForPublishers.filters',
    'agreementsForPublishers.services',
    'agreementsForPublishers.directives',
    'agreementsForPublishers.controllers',
    'ngRoute'
  ]);

app.config([
  '$routeProvider',
  function($routeProvider) {
    $routeProvider.when('/login', {
      templateUrl: 'partials/login.html',
      controller: 'CrossRefLoginController'
    });

    $routeProvider.when('/', {
      redirectTo: '/tc'
    });

    $routeProvider.when('/tc', {
      templateUrl: 'partials/license-list.html',
      controller: 'LicenseListController'
    });

    $routeProvider.when('/tc/new', {
      templateUrl: 'partials/license-edit.html',
      controller: 'LicenseEditController'
    });

    $routeProvider.when('/tc/:id', {
      templateUrl: 'partials/license-view.html',
      controller: 'LicenseViewController'
    });

    $routeProvider.when('/tc/:id/edit', {
      templateUrl: 'partials/license-edit.html',
      controller: 'LicenseEditController'
    });

    $routeProvider.when('/publisher-api-token', {
      templateUrl: 'partials/publisher-api-token.html',
      controller: 'ApiTokenController'
    });

    $routeProvider.when('/error/internal', {
      templateUrl: 'partials/error-internal.html'
    });

}]);

// Bring consts into root scope.
app.run(function ($rootScope) {
  $rootScope.consts = window.CROSS_REF;

  // For the nagivation menu.
  $rootScope.navClassFor = function(forPage, currentPage) {

    if (forPage == currentPage) {
      return "active";  
    }

    return "";
  }
});

// Intercept requests and log out on 401s.
app.config(function ($httpProvider) {
  var logsOutUserOn401 = ['$q', '$location', '$rootScope', function ($q, $location, $rootScope) {
    var success = function (response) {
      return response;
    };

    var error = function (response) {
      if (response.status === 401) {
        $rootScope.crossRefLoginState = false;
        $location.path('/login');
        return $q.reject(response);
      } 
      else {
        return $q.reject(response);
      }
    };

    return function (promise) {
      return promise.then(success, error);
    };
  }];

  $httpProvider.responseInterceptors.push(logsOutUserOn401);
});

// Ensure cookies get sent to and from the API.
app.config(['$httpProvider', function($httpProvider) {
  // Causes problems with CORS. See http://stackoverflow.com/questions/16661032/http-get-is-not-allowed-by-access-control-allow-origin-but-ajax-is
  delete $httpProvider.defaults.headers.common['X-Requested-With'];

  $httpProvider.defaults.withCredentials = true;  
}]);

// API for talking to licenses.
app.factory('LicensesAPI', function(Restangular) {
  return Restangular.withConfig(function(RestangularConfigurer) {
    RestangularConfigurer.withCredentials = true;  
    RestangularConfigurer.setBaseUrl(window.CROSS_REF.API_BASE);
    RestangularConfigurer.setDefaultHttpFields({
        withCredentials: true
    });
  });
});
