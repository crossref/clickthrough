'use strict';

var app = angular.module(
  'agreementsForResearchers',
  [
    'agreementsForResearchers.filters',
    'agreementsForResearchers.services',
    'agreementsForResearchers.directives',
    'agreementsForResearchers.controllers',
    'ngRoute'
  ]);

app.config([
  '$routeProvider',
  function($routeProvider) {
    $routeProvider.when('/login/:orcidUrl', {
      templateUrl: 'partials/login.html',
      controller: 'OrcidLoginController'
    });

    $routeProvider.when('/researcher-api-token', {
      templateUrl: 'partials/researcher-api-token.html',
      controller: 'ApiTokenController'
    });

    $routeProvider.when('/', {
      templateUrl: 'partials/review-list.html',
      controller: 'ReviewListController'
    });

    $routeProvider.when('/tc/:id*', {
      templateUrl: 'partials/review-view.html',
      controller: 'ReviewViewController'
    });

    $routeProvider.when('/error/internal', {
      templateUrl: 'partials/error-internal.html'
    });

    $routeProvider.when('/error/orcid', {
      templateUrl: 'partials/error-orcid.html'
    });  

    $routeProvider.when('/error/not-found', {
      templateUrl: 'partials/error-not-found.html'
    });  

    // $routeProvider.otherwise({redirectTo: '/error/not-found'});
}]);

// Handle the callback the ORCID iframe upon authentication.
app.run(function ($rootScope, $location, $window, AuthenticationService) {
  $window.addEventListener('message', function(e) {
    $rootScope.$apply(function(){
      var message = e.data;

      // Window will have closed itself.
    
      // If logged in OK, redirect back to the page on which the unauthenticated signal was raised.
      if (message === "ok") {
        var newLocation = $rootScope.authReferringRoute || "/";

        // This is the point at which the server has indicated that the authentication worked ok.
        AuthenticationService.orcid.setLoggedIn(true);
        $rootScope.hideMenubar = false;
        $location.path(newLocation);

      } else if (message === "internal-error") {
        $location.path("/error/internal")
      } else if (message === "orcid-error") {
        $location.path("/error/orcid")
      } else {
        $location.path("/error/internal")
      }
    });
  });
});

// TODO how to contain these?
var routesThatDontRequireAuth = ['/login', '/error'];

var routeClean = function (route) {
  return _.find(routesThatDontRequireAuth,
    function (noAuthRoute) {
      return _.str.startsWith(route, noAuthRoute);
    });
};

// Bring consts into root scope.
app.run(function ($rootScope) {
  $rootScope.consts = window.CROSS_REF;

  // When the redirect-to-login happens, the previous URL is stored for later redirection.
  $rootScope.authReferringRoute = null;

  // For the nagivation menu.
  $rootScope.navClassFor = function(forPage, currentPage) {
    if (forPage == currentPage) {
      return "active";  
    }

    return "";
  }
});


// Intercept requests and log out on 401s.
app.config(function ($httpProvider) {
  var logsOutUserOn401 = ['$q', '$location', '$rootScope', function ($q, $location, $rootScope) {
    var success = function (response) {
      return response;
    };


    var error = function (response) {      
      if (response.status === 401) {
        $rootScope.orcidLoginState = false;

        // The ORCID URL is returned as a string, or a JSON status object.
        if (response.data) {
          // We might get the ORCID redirect URL as a status object
          if (response.data.status && response.data['orcid-login-url']) {
            var orcidUrl = response.data['orcid-login-url'];
          // Or as the plain body text.
          } else if (response.data.substring(0, 4) == "http") {
            var orcidUrl = response.data;
          }

          // Save for later.
          var currentPath = $location.path();
          if (!routeClean(currentPath)) {
            $rootScope.authReferringRoute = currentPath;  
          }
          
          $location.path('/login/' + encodeURIComponent(orcidUrl));
          return $q.reject(response);
        } else {
          // If the server returns 401 the content should always be the redirect URL.
          // If not, that's an internal error.
         // $location.path("/error/internal")
        }
      } else if (response.status === 404) {
        $location.path("/error/not-found");
      } else {
        return $q.reject(response);
      }
    };

    return function (promise) {
      return promise.then(success, error);
    };
  }];

  $httpProvider.responseInterceptors.push(logsOutUserOn401);
});

// Ensure cookies get sent to and from the API.
app.config(['$httpProvider', function($httpProvider) {
  // Causes problems with CORS. See http://stackoverflow.com/questions/16661032/http-get-is-not-allowed-by-access-control-allow-origin-but-ajax-is
  delete $httpProvider.defaults.headers.common['X-Requested-With'];

  $httpProvider.defaults.withCredentials = true;  
}]);

// API for talking to reviews.
app.factory('ReviewsAPI', function(Restangular) {
  return Restangular.withConfig(function(RestangularConfigurer) {
    
    RestangularConfigurer.withCredentials = true;  
    RestangularConfigurer.setBaseUrl(window.CROSS_REF.API_BASE);
    RestangularConfigurer.setDefaultHttpFields({
        withCredentials: true
    });
  });
});
