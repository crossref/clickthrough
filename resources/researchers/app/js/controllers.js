'use strict';

/* Controllers */

var controllers = angular.module('agreementsForResearchers.controllers', ['restangular']);


// Log in for ORCID authentication.
controllers.controller('OrcidLoginController', function($scope, $http, $filter, $location, $rootScope, $routeParams, AuthenticationService) {
	$rootScope.currentPage = "login";

	$scope.username = "";
	$scope.password = "";

	// Don't show menubar on login screen. This is unset in the routing interceptor in app.js
	$rootScope.hideMenubar = true;

	$scope.errorMessage = null;

	// http://www.xtf.dk/2011/08/center-new-popup-window-even-on.html
	function PopupCenter(url, title, w, h) {
	    // Fixes dual-screen position                       Most browsers      Firefox
	    var dualScreenLeft = window.screenLeft != undefined ? window.screenLeft : screen.left;
	    var dualScreenTop = window.screenTop != undefined ? window.screenTop : screen.top;

	    var left = ((screen.width / 2) - (w / 2)) + dualScreenLeft;
	    var top = ((screen.height / 2) - (h / 2)) + dualScreenTop;
	    var newWindow = window.open(url, title, 'scrollbars=yes, width=' + w + ', height=' + h + ', top=' + top + ', left=' + left);

	    // Puts focus on the newWindow
	    if (window.focus) {
	        newWindow.focus();
	    }
	}

	// Hide the help paragraph until we've tried to show the popup.
	$scope.popupShown = false;
	$scope.showPopup = function () {
	 	$scope.popupShown = true;
		PopupCenter(decodeURIComponent($routeParams.orcidUrl), "Authenticate with ORCID", 1000, 900);
	}
});

controllers.controller('ApiTokenController', function($scope, $rootScope, $http, ReviewsAPI) {
	$rootScope.currentPage = "api-token";
		ReviewsAPI.one('researcher/my/researcher/token').get()
		.then(function(token) {
		  $scope.token = token;
		})
		// Revoke and re-fetch.
		$scope.revokeToken = function() {
			$http({method: 'DELETE', withCredentials: true, url: window.CROSS_REF.API_BASE + 'researcher/my/researcher/token'}).
				success(function(data, status, headers, config) {

					ReviewsAPI.one('researcher/my/researcher/token').get()
					.then(function(token) {
						$scope.token = token;
					})

	  			}).
				error(function(data, status, headers, config) {
					$location.path("/error/internal")
				});
			};
});

// Lists of license items.
controllers.controller('ReviewListController', function($scope, $rootScope, $filter, ReviewsAPI) {
	$rootScope.currentPage = "review-list";

	var reviews = ReviewsAPI.all("researcher/reviews");

	// The API is review-status oriented but here we want to display in global-oriented. So a bit of a pivot.
	// Each API response is async so ensure that the responses don't clobber each other's data.
	// Start with an empty list of licenses.
	$scope.globalLicenses = [];
	$scope.nonGlobalLicenses = [];

	$scope.orderNonGlobalByField = 'created_by_name';
	$scope.reverseNonGlobalSort = false;
	$scope.orderGlobalByField = 'title';
	$scope.reverseGlobalSort = false;

	// Look up each status from the relevant API endpoint.
	var lookups = [
		{api: "unreviewed", status: $scope.consts.STATUS_REVIEW_NOT_REVIEWED},
		{api: "accepted", status: $scope.consts.STATUS_REVIEW_ACCEPTED},
		{api: "postponed", status: $scope.consts.STATUS_REVIEW_POSTPONED},
		{api: "rejected", status: $scope.consts.STATUS_REVIEW_REJECTED}
	];

	_.each(lookups, function(lookup) {
		reviews.all(lookup.api).getList()
		.then(function(reviews) {
			reviews.forEach(function(review){
				review.status = lookup.status;
				if (review.global === true) {
					$scope.globalLicenses.push(review);
				} else {
					$scope.nonGlobalLicenses.push(review);
				}
			});
		});
	});
}); 

// Review license. 
controllers.controller('ReviewViewController', function($scope, $rootScope, $routeParams, $location, ReviewsAPI) {
	$rootScope.currentPage = "review-view";

	var ReviewsAPI = ReviewsAPI;

	var review = undefined;
	if ($routeParams.id !== undefined) {
		ReviewsAPI.one('researcher/licenses/review', $routeParams.id).get()
		.then(function(review) {

			// Restangular looks in the ID property to create the path.
			review.id = review.uri;
		 	$scope.review = review;
		})
	}

	$scope.accept = function() {
		if ($scope.review !== undefined) {
			if ($scope.review.status == $scope.consts.STATUS_REVIEW_NOT_REVIEWED || $scope.review.status == $scope.consts.STATUS_REVIEW_POSTPONED || $scope.review.status == $scope.consts.STATUS_REVIEW_REJECTED) {

				$scope.review.status = $scope.consts.STATUS_REVIEW_ACCEPTED;
				
				$scope.review.put().then(function() {
					$location.path("/");
				  }, function() {
				    $location.path("/error/internal")
				  });
			} else {
				// The button shouldn't have been clicked.
				$location.path("/error/internal")
			}
		};
	};
	
	$scope.postpone = function() {
		if ($scope.review !== undefined) {
			if ($scope.review.status == $scope.consts.STATUS_REVIEW_NOT_REVIEWED) {

				$scope.review.status = $scope.consts.STATUS_REVIEW_POSTPONED;
				
				$scope.review.put().then(function() {
					$location.path("/");
				  }, function() {
				    $location.path("/error/internal")
				  });
			} else {
				// The button shouldn't have been clicked.
				$location.path("/error/internal")
			}
		};
	};

	$scope.reject = function() {
		if ($scope.review !== undefined) {
			if ($scope.review.status == $scope.consts.STATUS_REVIEW_NOT_REVIEWED || $scope.review.status == $scope.consts.STATUS_REVIEW_POSTPONED) {

				$scope.review.status = $scope.consts.STATUS_REVIEW_REJECTED;
				
				$scope.review.put().then(function() {
					$location.path("/");
				  }, function() {
				    $location.path("/error/internal")
				  });
			} else {
				// The button shouldn't have been clicked.
				$location.path("/error/internal")
			}
		};
	};
});