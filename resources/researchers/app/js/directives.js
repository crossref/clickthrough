'use strict';

/* Directives */

var directives = angular.module('agreementsForResearchers.directives', []);

// Log out button. Calls the logout method when clicked.
directives.directive('logoutButton', function(AuthenticationService) {
  return {
	controller: function($scope, $element){
		$scope.logout = function(){

			// This will call the logout endpoint.
			// The response will trigger the redirect in the app.
     	AuthenticationService.orcid.logout();
		}
	},
	templateUrl: "partials/logout-button.html",
  link: function (scope, elem, attrs) {
	}
  }
});

directives.directive('reviewStatusBadge', function() {
    return {
        restrict: 'E',
        controller: function($scope) {
        },
        link: function(scope, element, attrs, $scope) {
          scope.statusText = "undefined";
          
          scope.$watch(attrs.review, function(review) {
            if(review) {
              
              switch (review.status) {
                  case scope.consts.STATUS_REVIEW_NOT_REVIEWED: 
                      scope.statusText = "Not yet reviewed"; break;
                  case scope.consts.STATUS_REVIEW_POSTPONED:
                      scope.statusText = "Postponed"; break;
                  case scope.consts.STATUS_REVIEW_REJECTED:
                      scope.statusText = "Rejected"; break;
                  case scope.consts.STATUS_REVIEW_ACCEPTED:
                      scope.statusText = "Accepted"; break;
              }
            }
          })
        },
        template: '{{statusText}}'
    };
});

directives.directive('usedByAuthors', function() {
    return {
        restrict: 'E',
        controller: function($scope) {
        },
        scope: {
          theLicense: '=license'
        },
        link: function(scope, element, attrs, $scope) {
          scope.statusText = "undefined";
          scope.fullViewState = false;
          
          scope.$watch('theLicense', function(theLicense) {
            if (theLicense) {

              var createdBy = null;
              if (!theLicense.global) {
                createdBy = theLicense.created_by_name;
              }

              // global_used_by could be null if it's not global.
              scope.names = _.filter(_.union(theLicense.global_used_by || [], createdBy), function(x) { return x && x.length > 1 });
            }
          })

          scope.toggle = function() { 
            scope.fullViewState = ! scope.fullViewState;
          }

        },
        templateUrl: 'partials/used-by-publisher-names.html'
    };
});


directives.directive('markdownView', function() {
    return {
        restrict: 'A',
        require: '?ngModel',
        link: function(scope, element, attrs, ngModel) {
            if (!ngModel) {
                return;
            }
 
            ngModel.$render = function() {
              var converter = new Showdown.converter();
              element.html(converter.makeHtml(ngModel.$viewValue || ''));
            };

        }
    };
});

directives.directive('sortMarker', ["$sce", function($sce) {
    return {
        restrict: 'E',
        scope: true,

        controller: function($scope) {
        },
        link: function(scope, element, attrs, $scope) {
          scope.order = undefined;
          scope.by = "";
          scope.down = false;
          scope.up = false;

          scope.column = attrs.column;

          scope.update = function() {
            if (attrs.column == scope.by) {
              if (scope.order === true) {            
                scope.up = true;
                scope.down = false;
              } else if (scope.order === false) {
                scope.up = false;
                scope.down = true;
              } 
            } else {
                scope.up = false;
                scope.down = false;
            }
          };

          scope.$watch(attrs.order, function(order) {
            scope.order = order;
            scope.update();
          })
        
          scope.$watch(attrs.by, function(by) {
            scope.by = by;
            $scope.test = scope.by;
            scope.update();
          }); 
        },
        template: '<span ng-show="down">&#9650;</span><span ng-show="up">&#9660;</span>'
    };
}]);