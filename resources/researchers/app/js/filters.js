'use strict';

/* Filters */

var filters = angular.module('agreementsForResearchers.filters', []);

// Convert a string ISO 8601 date and display it in a nice format.
filters.filter('niceDate', function() {
    return function(input) {
    
        return moment(input).format('MMMM Do YYYY, h:mm:ss a');

    }
  });


