'use strict';

/* Services */

var services = angular.module('agreementsForResearchers.services', []);

// Authentication service. Keep track of the login state.
app.factory('AuthenticationService', function($http, $location, $rootScope) {
  
  
  $rootScope.orcidLoginState |= undefined;

  // State for ORCID.
  var orcidState = {

    // Return true, false or undefined if not yet sure.
    isLoggedIn: function() {
      return $rootScope.orcidLoginState;
    },

    // Set login state.
    setLoggedIn: function(isLoggedIn) { 

      $rootScope.orcidLoginState = isLoggedIn;
      if (isLoggedIn) {
        $http({method: 'GET', withCredentials: true, url: window.CROSS_REF.API_BASE + "researcher/me"}).success(function(data) {
          if (data.status && data.status == "ok") {
             $rootScope.loggedInOrcid = data.user.orcid;
          }
        });
      } else {
        $rootScope.loggedInOrcid = null;
      }
    },

    // Log out with service. The response will invoke a route change.
    logout: function() {
      $http({
        method: 'GET',
        withCredentials: true,
        url: window.CROSS_REF.LOG_OUT
      });
      $rootScope.loggedInOrcid = null;
    }
  };

  // When setting up, attempt to talk to the authentication service.
  // The response handler will act based on this.
  $http({
    method: 'GET',
    withCredentials: true,
    url: window.CROSS_REF.LOG_IN_ORCID
  }).success(function(data, status, headers, config) {
    orcidState.setLoggedIn(true);
  }).error(function(data, status, headers, config) {
    orcidState.setLoggedIn(false);
  });


  return {
  	orcid: orcidState  
  };
});