var prospect = {
	sProspectServer: 'prospect.crossref.org',
	sORCID: '',
	sLicenseRef: '',

	initializeProspector: function()
	{
		this.sORCID =  $("#input-orcid").text();
		this.setupAcceptConfirm();
		this.setupRegenerateConfirm();
		this.preselectLicense();
	},

	preselectLicense: function()
	{
		var $preselect = '#' + $('#preselect').text();
		$($preselect).click();
	},

	setupAcceptConfirm: function()
	{
		var $p = $('<p>').text('Are you sure you want to acccept this license?');
		var $btnNo = $('<button>').addClass('btn').addClass('claim-no-btn').attr('id','cancelAccept').attr('onclick','prospect.cancelAccept()').text('No');
		var $btnOk = $('<button>').addClass('btn').addClass('btn-success').addClass('claim-ok-btn').attr('id','confirmAccept').attr('onclick','prospect.accept()').text('Yes');
		var $btns = $('<div>').addClass('btn-container').append($btnNo).append($btnOk);
		var $content = $('<div>').append($p).append($btns);
		$("#licenseAccept").popover({
					   title: 'Confirmation',
					   placement: 'top',
					   content: $('<div>').append($content).html(),
					   html: true,
		} );
	},
	setupRegenerateConfirm: function()
	{
		var $p = $('<p>').text('Are you sure you want to generate a new API token? Any applications using the old token will need to be updated to use the the new one.');
		var $btnNo = $('<button>').addClass('btn').addClass('claim-no-btn').attr('id','cancelRegenerate').attr('onclick','prospect.cancelRegenerate()').text('No');
		var $btnOk = $('<button>').addClass('btn').addClass('btn-success').addClass('claim-ok-btn').attr('id','confirmRegenerate').attr('onclick','prospect.regenerateToken()').text('Yes');
		var $btns = $('<div>').addClass('btn-container').append($btnNo).append($btnOk);
		var $content = $('<div>').append($p).append($btns);
		$("#regenerateAccept").popover({
					   title: 'Confirmation',
					   placement: 'top',
					   content: $('<div>').append($content).html(),
					   html: true,
		} );
	},
	cancelRegenerate: function()
	{
		$("#regenerateAccept").popover('toggle')
	},

	regenerateToken: function()
	{
		$.ajax({
			type: "POST",
			url: "/prospector/" + this.sORCID + "/regenerate_token",
			success: function( data ) {
				$("#input-api-token").text(data);
			}
		});
		$("#regenerateAccept").popover('toggle')
	},

	disableButton: function(selector)
	{
		selector.attr('disabled','');
	},

	enableButton: function(selector)
	{
		selector.removeAttr('disabled');
	},

	setupDialog: function(currentStatus)
	{
		switch(currentStatus)
		{
			case 'unread':
				this.enableButton($("#licenseDither"));
				this.enableButton($("#licenseReject"));
				this.enableButton($("#licenseAccept"));
				break;
			case 'read':
				this.enableButton($("#licenseDither"));
				this.enableButton($("#licenseReject"));
				this.enableButton($("#licenseAccept"));
				break;
			case 'accepted':
				this.disableButton($("#licenseDither"));
				this.disableButton($("#licenseReject"));
				this.disableButton($("#licenseAccept"));
				break;
			case 'rejected':
				this.disableButton($("#licenseDither"));
				this.disableButton($("#licenseReject"));
				this.enableButton($("#licenseAccept"));
				break;
			case 'preview':
				this.disableButton($("#licenseDither"));
				this.disableButton($("#licenseReject"));
				this.disableButton($("#licenseAccept"));
				break;
		}




	},

	showLicense: function(uri, currentStatus)
	{
		this.setupDialog(currentStatus);
		this.sLicenseRef = uri;
		$( "#dialog-form" ).modal({ 
			keyboard: true,
			remote: 'https://' + prospect.sProspectServer + '/license_details?license_uri=' + uri
		});
	},

	reject: function()
	{
		$.ajax({
			type: "POST",
			url: "/prospector/" + this.sORCID + "/reject?lic_ref=" + escape(this.sLicenseRef),
			success: function( data ) {
				prospect.reloadReviews();
			}
		});
		$( "#dialog-form" ).modal('hide').removeData();
	},

	cancelAccept: function()
	{
		$("#licenseAccept").popover('toggle')
	},

	accept: function()
	{
		$.ajax({
			type: "POST",
			url: "/prospector/" + this.sORCID + "/accept?lic_ref=" + escape(this.sLicenseRef),
			success: function( data ) {
				prospect.reloadReviews();
			}
		});
		$("#licenseAccept").popover('toggle')
		$( "#dialog-form" ).modal('hide').removeData();
	},

	dither: function()
	{
		$.ajax({
			type: "POST",
			url: "/prospector/" + this.sORCID + "/dither?lic_ref=" + escape(this.sLicenseRef),
			success: function( data ) {
				prospect.reloadReviews();
			}
		});
		$( "#dialog-form" ).modal('hide').removeData();
	},
	
	cancel: function()
	{
		$( "#dialog-form" ).modal('hide').removeData();
	},

	reloadReviews: function()
	{
		$.ajax({
			url: "/reviews/" + this.sORCID,
			success: function( data ) {
				var currentScroll = $("#reviews-table").scrollTop();
				$("#reviews-table").replaceWith(data);
				$("#reviews-table").scrollTop(currentScroll);
			}
		});
	},

	signOutOfORCID: function()
	{
		alert('dinkum');
		$.ajax({
			url: "https://orcid.org/userStatus.json?logUserOut=true&callback=exampleCallBack",
			datatype: "jsonp",
			success: function( data ) {
				alert(data);
			}
		});
	},

	/* Publisher Specific */

	initializePublisher: function()
	{
		this.setupDepositLoad();
	},

	setupDepositLoad: function()
	{
		$("#pick-button").click(function(e) {
			filepicker.pick({mimetypes: ['text/xml']},
					function(FPFile) {
						$.post("/deposit", {url: FPFile.url, filename: FPFile.filename}).done(function(data) {
							prospect.afterFileUpload(data);
						});
					});
					e.preventDefault();
					return false;
		});
	},

	afterFileUpload: function(data)
	{
		prospect.reloadLicenses();
		$("#load-status").html(data["load_status"]);
	},

	setupEditDialog: function()
	{
	},

	editLicense: function(uri, currentStatus)
	{
		this.setupEditDialog(currentStatus);
		this.sLicenseRef = uri;
		$( "#edit-dialog-form" ).modal({ 
			keyboard: true,
			remote: 'https://' + prospect.sProspectServer + '/edit_license?lic_ref=' + uri
		});
	},

	cancelEdit: function()
	{
		$( "#edit-dialog-form" ).modal('hide').removeData();
	},

	publishLicense: function()
	{
		$.ajax({
			type: "POST",
			url: '/publisher/publish/?lic_ref=' + escape(this.sLicenseRef),
			success: function( data ) {
				prospect.reloadLicenses();
			}
		});
		$( "#edit-dialog-form" ).modal('hide').removeData();
	},
	
	disableLicense: function()
	{
		$.ajax({
			type: "POST",
			url: '/publisher/disable/?lic_ref=' + escape(this.sLicenseRef),
			success: function( data ) {
				prospect.reloadLicenses();
			}
		});
		$( "#edit-dialog-form" ).modal('hide').removeData();
	},
	
	reloadLicenses: function()
	{
		$.ajax({
			url: "/licenses/",
			success: function( data ) {
				var currentScroll = $("#licenses-table").scrollTop();
				$("#licenses-table").replaceWith(data);
				$("#licenses-table").scrollTop(currentScroll);
			}
		});
	},

};

